﻿$(window).load(function () {
    var gamehub = $.connection.GameTicTacHub;
    var userID = $("#user-id").val();
    var gamenew;
    var bot;

    gamehub.client.enteruserhub = function (enterUser) {
        if (enterUser.UserID != userID) {
            UpdateOnlineGamer("-1", enterUser);
        }
    };

    gamehub.client.tryinvite = function (result) {
        if (result.IsInvite && userID == result.Game.SecondUserID) {
            AppendToDiscussion(result.InviterName + " " +
                htmlLocal("yougetinvite") + ": " + result.Game.SecondUserName
                + ", " + htmlLocal("WantGame") + "?");

            UpdateOnlineInvite(result.Game, "unstarted", true);
        }

        if (result.IsInvite && userID == result.InviterUserID) {
            AppendToDiscussion(htmlLocal("invitearriwefor") + " " + result.Game.SecondUserName);
        }
        if (!result.IsInvite && userID == result.InviterUserID) {
            AppendToDiscussion("Ошибка приглашения: " + result.Error);
        }
    };

    gamehub.client.agree = function (gameID, inviterUserID, inviterUserName, secondUserID, secondUserName, fieldSize) {
        if (userID == inviterUserID || !gamenew) {
            UpdateStatus(htmlLocal("heagree") + " (" + secondUserName + "). " + htmlLocal("gamestarted"));
            gamenew = new Game(userID, inviterUserID, inviterUserName, secondUserID, secondUserName, false, gameID, fieldSize);
            gamenew.StartGame();
            $("#user-gameid").val(gameID);
            gamenew.IsTurned = true;
        } else {
            var findinvite = $("#invites #invites-" + gameID).length;
            if (findinvite != 0 && userID == secondUserID) {
                $("#invites #invites-" + gameID).slideUp(1000, function () {
                    $(this).remove();
                    CountDecrease("#invitesunstartedcount span");
                });
            }
        }
        SetBusyOnlineGamer(secondUserID);
        SetBusyOnlineGamer(inviterUserID);
    };

    gamehub.client.refuse = function (gameID, inviterUserID, inviterName, secondUserID, secondUserName) {
        var findinvite = $("#invites #invites-" + gameID).length;
        if (findinvite != 0 && secondUserID == userID) {
            $("#invites #invites-" + gameID).slideUp(1000, function () {
                $(this).remove();
                CountDecrease("#invitesunstartedcount span");
            });
        }

        if (inviterUserID == userID) {
            UpdateStatus(secondUserName + " " + _R("refused"));
        }
        if (secondUserID == userID) {
            AppendToDiscussion(_R("unfortunaterefuse"));
        }
    };

    gamehub.client.tryturn = function (gameID, authorUserID, x, y, gameIsStopped, winnerUserID, inviterUserID, secondUserID) {
        if (gameIsStopped) {
            SetFreeOnlineGamer(inviterUserID);
            SetFreeOnlineGamer(secondUserID);
        }

        if (gamenew && gamenew.GameID == gameID && gamenew.CurrentUserID == userID) {
            if (userID != authorUserID) {
                gamenew.DrawOpponentPoint(x, y);
                gamenew.IsTurned = false;
                gamenew.WinnerUserID = winnerUserID;
                gamenew.GameIsStopped = gameIsStopped;
                gamenew.GameIsStarted = !gameIsStopped;
            }
            gamenew.NoWinnerEnded = winnerUserID == "0";
            gamenew.VictoryEnded = winnerUserID != "0";

            if (gamenew.GameIsStopped) {
                gamenew.ClearGame();
                if (gamenew.InviterUserID == userID) {
                    var aggInvite = $("#onliners .onliners-gamer[data-secuserid='" + gamenew.SecondUserID + "']");
                    aggInvite.data("repeat", true);
                    aggInvite.click();
                }

                gamenew = null;
            }
            else {
                if (userID != authorUserID) {
                    if (gamenew.isX)
                        UpdateStatus(htmlLocal("turntick"));
                    else
                        UpdateStatus(htmlLocal("turntack"));
                } else {
                    UpdateStatus(htmlLocal("statusawaitofuser") + "...");
                }
            }
        }
    };

    gamehub.client.tryofferstandoff = function (gameID, initiatorLogOffUserID, initiatorName) {
        if (gamenew) {
            if (gamenew.CurrentUserID == userID) {
                if (gamenew.CurrentUserID != initiatorLogOffUserID) {
                    $(".game-rule-nowin").fadeOut(100, function () {
                        UpdateStatus(initiatorName + " " + _R("SuggsestNoWin"));
                        $(".game-rule-nowin-agree").fadeIn(100);
                    });
                }
                if (gamenew.CurrentUserID == initiatorLogOffUserID) {
                    $(".game-rule-nowin").fadeOut(100, function () {
                        UpdateStatus(_R("SuggestGoToGamer"));
                    });
                }
            }
        }
        SetFreeOnlineGamer(initiatorLogOffUserID);
    };

    gamehub.client.trystandoffagree = function (gameID) {
        if (gamenew && gamenew.GameID == gameID) {
            gamenew.NoWinnerEnded = true;
            gamenew.ClearGame();
            gamenew = null;
        }
        //SetFreeOnlineGamer(initiatorLogOffUserID);
    };

    gamehub.client.trygameoff = function (gameID, logofferUserID, logofferUserName, winUserID) {
        if (gamenew && gamenew.GameID == gameID) {
            gamenew.WinnerUserID = winUserID;
            gamenew.GameIsStopped = true;
            gamenew.VictoryEnded = true;

            var currentUser = gamenew.CurrentUserID;
            gamenew.ClearGame();
            gamenew = null;
            if (currentUser == userID && logofferUserID != userID) {
                UpdateStatus(_R("exitgamer") + " " + logofferUserName);
            }
        }

        UpdateOnlineGamer("-1", { UserID: logofferUserID, IsOnline: false });
    };

    // Start the connection.
    $.connection.hub.start().done(function () {
        $(document).on("click", ".enteruseraction", function (e) {
            gamehub.server.send(userID);

            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", "#onliners div.onliners-gamer", function (e) {
            var action = $(this);
            var isonline = action.find(".online");
            var statusOnline = "офф-лайн";
            if (isonline && isonline.length != 0) {
                statusOnline = "он-лайн";
            }
            var isrepeat = action.data("repeat");
            var secondUserName = action.data("secname");
            var askfield = _R("WriteFieldSize");
            var fullquestion = secondUserName + " " + statusOnline + ". " + _R("youwantinvite") + " " + secondUserName + "? " + askfield;
            if (isrepeat) {
                var fullquestion = _R("youwantinviterepeat") + " " + secondUserName + "? " + askfield;
            }

            var lastfs = $("#user-lastfs").val();
            var fs = prompt(fullquestion, lastfs);

            var fieldSize = +fs;
            if (fieldSize && fieldSize >= 3 && fieldSize < 25) {
                $("#user-lastfs").val(fieldSize);
                $(".game-table div").removeClass("game-table-o").removeClass("game-table-x");
                UpdateStatus(htmlLocal("youinvite") + " " + secondUserName);
                var secondUserID = action.data("secuserid");
                gamehub.server.invite(userID, secondUserID, fieldSize);
            } else {
                if (fs != null) {
                    alert(_R("tryagain"));
                }
            }

            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", "a.agree-invite-action", function (e) {
            var action = $(this);
            var answer = false;
            if (gamenew) {
                var q = _R("areyousure");
                answer = confirm(q);
                if (answer) {
                    gamenew.ClearGame();
                }
            }
            if (!gamenew || answer) {
                var inviterUserID = action.data("inviterid");

                var isOnline = GamerIsOnline(inviterUserID);
                var secondUserName = action.data("secondusername");
                if (isOnline) {
                    var secondUserID = action.data("seconduserid");
                    // вся логика должна быть перенесена в gamehub.client.agree
                    if (userID == secondUserID) {
                        var inviterUserName = action.data("invitername");
                        var gameID = action.data("gameid");
                        var fieldSize = action.data("fieldsize");

                        gamenew = new Game(userID, inviterUserID, inviterUserName, secondUserID, secondUserName, true, gameID, fieldSize);
                        gamenew.StartGame();
                        $("#user-gameid").val(gameID);

                        UpdateStatus(_R("youagreego") + ", " + secondUserName + "!");

                        gamehub.server.agree(gameID, inviterUserID, inviterUserName, secondUserID, secondUserName, fieldSize);
                    }
                } else {
                    UpdateStatus(_R("exitgamer") + " " + secondUserName);
                }
            }
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", "a.refuse-invite-action", function (e) {
            var inviterUserID = $(this).data("inviterid");
            var inviterUserName = $(this).data("invitername");
            var secondUserID = $(this).data("seconduserid");
            var secondUserName = $(this).data("secondusername");
            var gameID = $(this).data("gameid");
            gamehub.server.refuse(gameID, inviterUserID, inviterUserName, secondUserID, secondUserName);

            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", ".game-table div", function (e) {
            if (bot) {
                alert("Сейчас вы играете с ботом");
            } else {

            }
            var el = $(this);
            if (!gamenew) {
                UpdateStatus(_R("gamenullable"));
                $(".game-field").addClass("game-field-none");
            }
            else {
                if (gamenew && !gamenew.GameIsStopped) {

                    if (el.hasClass("game-table-o") || el.hasClass("game-table-x")) {
                        alert("К сожалению, сюда нельзя нажимать");
                    } else {
                        if (!gamenew.IsTurned && gamenew.GameIsStarted) {
                            var x = el.data("x"),
                                y = el.data("y");

                            gamenew.DrawMyPoint(x, y);
                            gamenew.IsTurned = true;
                            gamenew.Points.push({ "x": x, "y": y });

                            gamenew.VictoryEnded = gamenew.VerifyVictory(); // окончилась ли сейчас победой

                            if (!gamenew.VictoryEnded) {
                                gamenew.NoWinnerEnded = gamenew.VerifyNoExistTurn(); // проверка есть ли ходы еще!
                            }

                            gamehub.server.turn(gamenew.GameID, gamenew.CurrentUserID, x, y, gamenew.GameIsStopped, gamenew.WinnerUserID, gamenew.InviterUserID, gamenew.SecondUserID);
                        }
                        else {
                            UpdateStatus(_R("atemptawaitturnopp") + "...");
                        }
                    }
                }
                else {
                    UpdateStatus("Уже выиграна партия");
                }
            }
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("mouseenter", ".game-table div", function (e) {
            if (gamenew && !gamenew.IsTurned) {
                gamenew.Mouseenter($(this));
            }
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("mouseleave", ".game-table div", function (e) {
            if (gamenew) {
                gamenew.Mouseleave($(this));
            }
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", ".game-rule-giveup", function (e) {
            if (gamenew) {
                var winnerUserID = gamenew.InviterUserID;
                if (winnerUserID == userID)
                    winnerUserID = gamenew.SecondUserID;
                gamehub.server.finished(gamenew.GameID, userID, winnerUserID);
            }
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", ".game-rule-nowin", function (e) {
            if (gamenew) {
                gamehub.server.standOff(gamenew.GameID, userID, $("#user-name").val());
            }
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", ".game-rule-nowin-agree", function (e) {
            if (gamenew) {
                gamehub.server.agreeStandoff(gamenew.GameID);
            }
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on("click", "#withbot a", function (e) {
            alert("Вы начали играть с ботом");
            var fieldSize = 1;
            var isX = true;
            bot = new Bot($("#user-id").val(), fieldSize, isX);

            if (bot.gamenew)
                gamenew.IsGameWithBot = true;

            e.preventDefault();
            e.stopPropagation();
        });

        $(window).bind("beforeunload", function (e) {
            e = e || window.event;
            if (gamenew) {
                var winnerUserID = gamenew.InviterUserID;
                if (winnerUserID == userID)
                    winnerUserID = gamenew.SecondUserID;

                gamehub.server.finished(gamenew.GameID, userID, winnerUserID);
            }

            e.preventDefault();
            e.stopPropagation();
        });

    });
});

