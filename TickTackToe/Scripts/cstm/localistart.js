﻿$(document).ready(function () {
    $('body').animate({ opacity: 0.8 }, 'fast', function () {
        $(this).addClass("main-bg").animate({ opacity: 1 });
    });

    $("input[type='radio']").click(function () {
        $(this).parents("form").submit(); // post form
    });
});

function _R(id) {
    var localWord = GetResources(id);

    return localWord || id;
}

function GetResources(id) {
    var url = '/base/GetResource';
    var json = (function () {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': url,
            'dataType': "json",
            'data': { id: id },
            'success': function (data) {
                json = data;
            }
        });
        return json;
    })();
    return json;
}