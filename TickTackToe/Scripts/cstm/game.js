﻿function Game(_currentUserID, _inviterUserID, _inviterName, _secondUserID, _secondUserName, _isX, _gameID, _fieldSize) {
    this.Points = [];

    this.CurrentUserID = _currentUserID;
    this.InviterUserID = _inviterUserID;
    this.InviterName = _inviterName;
    this.SecondUserID = _secondUserID;
    this.SecondUserName = _secondUserName;
    this.isX = _isX;
    this.GameID = _gameID;
    this.FieldSize = _fieldSize;
    this.WinnerCompositionSize = 3;
    this.CellsCount = _fieldSize * _fieldSize;

    this.WinnerUserID = "0";
    this.GameIsStarted = false;
    this.GameIsStopped = false;
    this.VictoryEnded = false;
    this.NoWinnerEnded = true;
    this.IsTurned = false;
    this.IsGameWithBot = false;


    this.StartGame = function () {
        this.WinnerCompositionSize = this.FieldSize;
        if (this.FieldSize > 4) {
            if (this.FieldSize > 7) {
                this.WinnerCompositionSize = 5;
            }
            else {
                this.WinnerCompositionSize = 4;
            }
        }

        if (this.FieldSize > 10) {
            if ($(".log-div").hasClass("col-md-4")) {
                $(".log-div").toggleClass("col-md-4 col-md-2");
                $(".game-div").toggleClass("col-md-5 col-md-7");
            }
        }
        else {
            if ($(".log-div").hasClass("col-md-2")) {
                $(".log-div").toggleClass("col-md-2 col-md-4");
                $(".game-div").toggleClass("col-md-7 col-md-5");
            }
        }

        drawCustomField(this.FieldSize);
        $(".game-rule").slideDown(1000, function () {
            $(".game-rule-nowin").slideDown(1000);
            $(".game-rule-nowin-agree").slideUp(1000);
            scrollToGameField();
        });

        var fieldInfo = " (" + _R("fieldsize") + " " + this.FieldSize + " : " + _R("winnercompsize") + " " + this.WinnerCompositionSize + ")";
        if (this.isX) {
            $("table").addClass("game-table-bg");
            $(".game-opponent span").html(this.InviterName + fieldInfo);
        }
        else {
            $("table").removeClass("game-table-bg");
            $(".game-opponent span").html(this.SecondUserName + fieldInfo);
        }

        $(".game-table div").removeClass("game-table-o").removeClass("game-table-x");

        if ($(".game-field").hasClass("game-field-none")) {
            $(".game-field").slideDown(1200, function () {
                $(".game-field").removeClass("game-field-none");
            });
        }

        var status = _R("gamestartwith") + " ";
        if (!this.isX)
            UpdateStatus(status + this.SecondUserName);
        else
            UpdateStatus(status + this.InviterName);

        this.GameIsStarted = true;
    };

    this.DrawMyPoint = function (x, y) {
        drawPoint(x, y, this.isX);
    };
    
    this.DrawOpponentPoint = function (x, y) {
        drawPoint(x, y, !this.isX);
    };

    this.Mouseenter = function (that) {
        var contentbgr = "game-table-o-hover";

        if (this.isX) {
            contentbgr = "game-table-x-hover";
        }

        var isx = $(that).hasClass("game-table-x");
        var iso = $(that).hasClass("game-table-o");
        if (!isx && !iso)
            $(that).addClass(contentbgr);
    };

    this.Mouseleave = function (that) {
        $(that).removeClass("game-table-x-hover").removeClass("game-table-o-hover");
    };

    this.VerifyVictory = function () {
        var points = this.Points,
        l = points.length,
        isvictory = false,
        ws = this.WinnerCompositionSize; // по дефолту 3;;

        if (l >= this.WinnerCompositionSize) {
            isvictory = searchDiagonal(this);

            if (!isvictory) {
                var p = points[l - 1],
                from_x = p.x - ws + 1,
                to_x = p.x + ws - 1,
                from_y = p.y - ws + 1,
                to_y = p.y + ws - 1;

                if (search(this, from_x, to_x, p.y, "x", "y") || search(this, from_y, to_y, p.x, "y", "x")) {
                    isvictory = true;
                }
            }
        }
        if (isvictory) {
            this.GameIsStarted = false;
            this.GameIsStopped = true;
            this.WinnerUserID = this.CurrentUserID;
        }

        return isvictory;
    };

    this.VerifyNoExistTurn = function () {
        var oCount = $(".game-table div.game-table-o").length;
        var xCount = $(".game-table div.game-table-x").length;

        var num = oCount + xCount;
        var isNo = num == this.CellsCount;
        if (isNo) {
            this.GameIsStarted = false;
            this.GameIsStopped = true;
        }
        return isNo;
    };

    this.ClearGame = function () {
        if (this.VictoryEnded) {
            var iswinner = this.WinnerUserID == this.CurrentUserID;
            if (iswinner) {
                info(htmlLocal("statuswin"));
                alert(_R("statuswin"));
                CountIncrease("#victories span");
              //  updateCount("#victories");
            } else {
                info(htmlLocal("statuslose"));
                alert(_R("statuslose"));
                CountIncrease("#losses span");
              //  updateCount("#losses");
            }
        } else {
            info(htmlLocal("statusno"));
            alert(_R("statusno"));
        }

        AppendToDiscussion("");

        $(".game-rule").slideUp(100);

        if ($(".discussion li").length > 20) {
            $(".discussion").empty();
        }
    };

    function info(status) {
        UpdateStatus(htmlLocal("statusgamestop") + ". " + status + ". " + htmlLocal("youinviteagain") + ".");
        UpdateStatus(status + "!");
    }

    function search(that, from, to, constval, firstproperty, secondporperty) {
        var points = that.Points,
            fs = that.FieldSize,
            sovp = 0,
            ws = that.WinnerCompositionSize; // по дефолту 3;

        for (var sv = from; sv < to + 1; sv++) {
            if (sv < 0 || sv >= fs) {
                continue;
            }

            var result = arrObjIndexOf(points, sv, constval, firstproperty, secondporperty); // 1
            if (result == -1) {
                sovp = 0;
                continue;
            }
            else {
                sovp++;
                if (sovp == ws) {
                    UpdateStatus(htmlLocal("statuswin") + "!!!");
                    return true;
                }
            }
        }
    }

    function searchDiagonal(that) {
        var points = that.Points,
            fs = that.FieldSize,
            ws = that.WinnerCompositionSize, // по дефолту 3
            p = points[points.length - 1],
            from_x = p.x - ws + 1,
            to_x = p.x + ws,
            from_y = p.y - ws + 1,
            to_y = p.y + ws - 1;

        var ltr = srchDiagLTR(from_x, to_x, to_y, fs, ws, points);
        var rtl = srchDiagRTL(from_x, to_x, from_y, fs, ws, points);

        return ltr || rtl;
    }

    function srchDiagLTR(from_x, to_x, to_y, fs, ws, points) {
        var diagArr = [],
            sovp = 0;
        for (var i = from_x; i < to_x; i++) {
            if (i < 0 || to_y < 0 || i >= fs || to_y >= fs) {
                to_y--;
                continue;
            }

            diagArr[i] = indexOfDiag(points, i, to_y);
            if (diagArr[i]) {
                sovp++;
                if (sovp == ws) {
                    return true;
                }
            } else {
                sovp = 0;
            }

            to_y--;
            if (to_y < 0) {
                break;
            }
        }
    }

    function srchDiagRTL(from_x, to_x, from_y, fs, ws, points) {
        var sovp = 0,
         diagArr = [];

        for (var k = from_x; k < to_x; k++) {
            if (k < 0 || from_y < 0 || k >= fs || from_y >= fs) {
                from_y++;
                continue;
            }

            diagArr[k] = indexOfDiag(points, k, from_y);
            if (diagArr[k]) {
                sovp++;
                if (sovp == ws) {
                    return true;
                }
            } else {
                sovp = 0;
            }

            from_y++;
            if (from_y >= fs) {
                break;
            }
        }

        return false;
    }

    function indexOfDiag(points, x, y) {
        return (arrObjIndexOf(points, x, y, "x", "y") != -1);
    }

    function arrObjIndexOf(myArray, searchValue, secondValue, firstproperty, secondporperty) {
        for (var i = 0, len = myArray.length; i < len; i++) {
            if (myArray[i][firstproperty] === searchValue && myArray[i][secondporperty] === secondValue) return i;
        }
        return -1;
    }

    function drawCustomField(fieldSize) {
        $(".game-table tbody").empty();
        for (var i = 0; i < fieldSize; i++) {
            var tr = $("<tr/>");
            for (var j = 0; j < fieldSize; j++) {
                var div = $("<div/>", {
                    "data-x": i,
                    "data-y": j,
                });

                var td = $("<td/>").append(div);
                tr.append(td);
            }
            $(".game-table tbody").append(tr);
        }
    }

    function drawPoint(x, y, isX) {
        var contentbgr = "game-table-o";

        if (isX) {
            contentbgr = "game-table-x";
        }

        var dest_xy = $('.game-table tbody td div[data-x="' + x + '"][data-y="' + y + '"]');

        dest_xy.addClass(contentbgr);
    };

    //function updateCount(id) {
    //    var currval = $(id + " span").text();
    //    var count = +currval;
    //    if (count) {
    //        count++;
    //        $(id + " span").text(count);
    //    }
    //}

    function scrollToGameField() {
        try {
            $("html, body").delay(500).animate({ scrollTop: $('.game-div').offset().top - 10 }, 'slow');
        } catch (e) {
            console.log(e);
        }
    }
}

