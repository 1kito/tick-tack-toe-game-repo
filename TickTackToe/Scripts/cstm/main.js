﻿$(window).load(function () {
    var _urlcommon = "/Home/GetCommonInfo";
    var _urlinfo = "/Home/GetUserInfo";
    GetJson(_urlinfo, UpdateUserInfo);
    GetJson(_urlcommon, UpdateCommonInfo);
    setTimeout(function () {
        $(".enteruseraction").click();
    }, 1000);

    $(document).on("mouseenter", ".onliners-gamer .g-info-f", function (e) {
        var info_f = $(this);
        var info_s = $(this).parent().find(".g-info-s");
        info_f.fadeOut(300, function () {
            info_s.fadeIn(300);
        });

        e.preventDefault();
        e.stopPropagation();
    });

    $(document).on("mouseleave", ".onliners-gamer .g-info-s", function (e) {
        var info_f = $(this).parent().find(".g-info-f");
        var info_s = $(this);
        info_s.fadeOut(300, function () {
            info_f.fadeIn(300);
        });

        e.preventDefault();
        e.stopPropagation();
    });

    setInterval(function () {
        GetJson(_urlcommon, UpdateCommonInfo);
    }, 59000);
});


function UpdateUserInfo(data) {
    $("#user-id").val(data.UserID);
    $("#user-name").val(data.Name);
    $("#name span").text(data.Name);
    $("#email span").text(data.UserName);
    $("#age span").text(data.Age);
    $("#regtime span").text(data.RegisterTime);
}

function UpdateCommonInfo(data) {
    UpdateOnlineInvites(data.InvitesUnStarted, "unstarted");
    UpdateOnlineInvites(data.InvitesUnFinished, "unfinished");

    $("#invitesunstartedcount span").text(data.InvitesUnStarted.length);
    $("#invitesunfinishedcount span").text(data.InvitesUnFinished.length);

    $(".user-games #userscount span").text(data.AllUsersCount);
    $(".user-games #onlinerscount span").text(data.OnlineUsersCount);

    $("#victories span").text(data.WinCount);
    $("#losses span").text(data.LoseCount);


    $.each(data.OnlineGamers, UpdateOnlineGamer);
    DeleteOfflineGamers(data.OnlineGamersUserIDs);
}

function UpdateOnlineGamer(i, key) {
    var findonliner = $("#onliners #onliners-" + key.UserID);

    if (findonliner.length == 0) {

        if (key.IP == "::1") key.IP = "localhost";
        if (!key.IP) key.IP = "не известен";
        if (i == "-1") {
            var c = $(".user-games #onlinerscount span").text();
            var counton = +c;
            counton++;
            $(".user-games #onlinerscount span").text(counton);
            AppendToDiscussion(_R("enternewuser") + " " + key.Name);
        }

        var div = $("<div/>", {
            "id": "onliners-" + key.UserID,
            "class": "onliners-gamer",
            "data-secname": key.Name,
            "data-secuserid": key.UserID,
            "data-repeat": false,
        });

        var bname = $("<div/>", {
            "text": key.Name
        });

        var div_xs_5_name = $("<div/>",
            {
                "class": "col-xs-5"
            }).append(bname);
        var inviteAction = $("<a/>", {
            "text": _R("InviteActionLabel"),
            "href": "#",
            "class": "invite-action onliners-gamer-invite-action btn btn-default"
        });

        var iconuser = $("<i/>", { "class": "glyphicon glyphicon-user onliners-gamer-icon" });
        SetIconStatusOnlineGamer(iconuser, key.IsGamed, key.IsOnline);
        if (!key.IsOnline) {
            inviteAction.addClass("hide");
        }
        var div_xs_2 = $("<div/>",
          {
              "class": "col-xs-2"
          }).append(iconuser);
        var row = $("<div/>", { "class": "row" });
        row.append(div_xs_2);

        var invite = $("<div/>", {
            "class": "btn-group btn-group-xs"
        }).append(inviteAction);
        div_xs_5_name.append(invite);
        row.append(div_xs_5_name);
        if (key.LoseCount || key.LoseCount == 0) {
            var div_xs_5 = $("<div/>", { "class": "col-xs-5" });
            var span_lasted = $("<span/>", {
                "text": key.Lasted,
                "title": "дата последней игры",
                "class": "g-info g-info-f"
            });
            var span_regtime = $("<span/>", {
                "text": key.RegTime,
                "title": "дата регистрации",
                "class": "g-info g-info-s"
            });
            var div_winlose_count = $("<div/>", {
                "text": key.WinCount + " / " + key.LoseCount,
                //"class": "label span-bg-user-info",
                "title": "победы/поражения"
            });
            div_xs_5.append(span_lasted).append(span_regtime).append(div_winlose_count);
            row.append(div_xs_5);
        }
        div.append(row);
        $("#onliners").append(div);
    } else {
        var findusericon = findonliner.find(".onliners-gamer-icon");
        var finduseractioninvite = findonliner.find(".invite-action");
        if (findusericon && finduseractioninvite) {

            SetIconStatusOnlineGamer(findusericon, key.IsGamed, key.IsOnline);
            if (key.IsOnline) {
                finduseractioninvite.removeClass("hide");
            } else {
                finduseractioninvite.addClass("hide");
            }
        }
    }
}

function DeleteOfflineGamers(onlineGamersUserIDs) {
    var existgamers = $("#onliners .onliners-gamer");

    $.each(existgamers, function (i, key) {
        var id = $(key).data("secuserid");
        var indexUserID = onlineGamersUserIDs.indexOf(id);

        if (indexUserID == -1) {
            var name = $(key).data("secname");
            AppendToDiscussion(_R("exitgamer") + " " + name);
            $(key).remove();
        }
    });
}


function UpdateOnlineInvites(invites, subidinvite) {
    $.each(invites, function (i, invite) {
        UpdateOnlineInvite(invite, subidinvite);
    });
}

function UpdateOnlineInvite(invite, subidinvite, isAddCount) {
    var findinvite = $("#invites #invites-" + invite.ID);
    var isgamed = false; // узнаем не играет ли сейчас эта игра
    var gameID = $("#user-gameid").val();
    if (gameID) {
        var isgamed = invite.ID == gameID;
    }
    if (findinvite.length == 0 && !isgamed) {
        var div = $("<div/>", {
            "id": "invites-" + invite.ID,
            "class": "invites-on-game row"
        });

        var div_left = $("<div/>", {
            "class": "col-xs-7"
        });
        var div_right = $("<div/>", {
            "class": "col-xs-5"
        });

        var span = $("<span/>").html(_R("YouInvited") + " <b>" + invite.InviterName + "</b>");
        div_left.append(span);
        div.append(div_left);

        var startAction = $("<a/>", {
            "text": _R("StartGame"),
            "href": "#",
            "class": "agree-invite-action btn btn-default",
            "data-gameid": invite.ID,
            "data-invitername": invite.InviterName,
            "data-inviterid": invite.InviterUserID,
            "data-secondusername": invite.SecondUserName,
            "data-seconduserid": invite.SecondUserID,
            "data-fieldsize": invite.FieldSize || 3
        });

        if (subidinvite == "unfinished") {
            startAction.text(_R("ContinueGame"));
        }

        var refusingAction = $("<a/>", {
            "text": _R("RefuseGame"),
            "href": "#",
            "class": "refuse-invite-action btn btn-default",
            "data-invitername": invite.InviterName,
            "data-inviterid": invite.InviterUserID,
            "data-gameid": invite.ID,
            "data-secondusername": invite.SecondUserName,
            "data-seconduserid": invite.SecondUserID,
        });

        var actionStartDiv = $("<div/>", { "class": "btn-group btn-group-xs" }).append(startAction);
        var actionRefuseDiv = $("<div/>", { "class": "btn-group btn-group-xs invites-on-game-refuse" }).append(refusingAction);
        div_right.append(actionStartDiv);
        div_right.append(actionRefuseDiv);
        div.append(div_right);

        $("#invites #" + subidinvite).append(div);

        if (isAddCount) {
            CountIncrease("#invitesunstartedcount span");
        }
    }
};

