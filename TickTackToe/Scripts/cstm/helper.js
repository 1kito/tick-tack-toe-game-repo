﻿function GamerIsOnline(userID) {
    var findonliner = $("#onliners #onliners-" + userID);
    if (findonliner) {
        var findusericon = findonliner.find(".onliners-gamer-icon");

        if (findusericon && findusericon.hasClass("online")) {
            return findusericon;
        }
    }
    return false;
}

function getIcon(userID) {
    var usericon = userID;
    if (jQuery.type(userID) === "string") {
        usericon = GamerIsOnline(userID);
    }
    return usericon;
}

function SetBusyOnlineGamer(userID) {
    var usericon = getIcon(userID);

    if (usericon) {
        usericon.prop({ "title": "занят" });
        usericon.removeClass("online-free").addClass("online-busy");
    }
}

function SetFreeOnlineGamer(userID) {
    var usericon = getIcon(userID);

    if (usericon) {
        usericon.prop({ "title": "свободен" });
        usericon.removeClass("online-busy").addClass("online-free");
    }
}

function SetIconStatusOnlineGamer(userID, isGamed, isOnline) {
    var usericon = getIcon(userID);

    if (isOnline) {
        if (isGamed) {
            SetBusyOnlineGamer(usericon);
        } else {
            SetFreeOnlineGamer(usericon);
        }
        usericon.addClass("online");
    } else {
        usericon.prop({ "title": "офф-лайн" });
        usericon.removeClass("online online-free online-busy");
    }
}

function CountIncrease(selector) {
    var c = $(selector).text();
    var n = +c;
    if (n || n == 0)
        $(selector).text(n + 1);
}

function CountDecrease(selector) {
    var c = $(selector).text();
    var n = +c;
    if (n)
        $(selector).text(n - 1);
}


function AppendToDiscussion(text) {
    var lis = $("#discussion li");
    if (lis) {
        var last = lis[0];

        if (!last || (last && text != $(last).text())) {
            var li = $("<li/>").html(text);
            li.prependTo($("#discussion"));
        }
    }

    lis = $("#discussion li");

    var sup = 30;
    var deletesince = 15;
    if (lis.length > sup) {
        for (var i = deletesince; i < lis.length; i++) {
            lis[i].remove();
        }
    }
}

function UpdateStatus(text) {
    $(".game-status").fadeOut(200, function () {
        $(".game-status").html(text);
        $(".game-status").fadeIn(500);
    });

    AppendToDiscussion($(".game-status").html());
}


function htmlLocal(datavalueid) {
    var text = _R(datavalueid);

    var span = $('<span />', {
        "text": text,
        "data-l": datavalueid
    });

    var wrapper = $("<p/>");

    wrapper.append(span);
    return wrapper.html();
}

function GetJson(_url, callback, params) {
    // params = params || {};
    $.ajax({
        type: "Get",
        url: _url,
        data: params,
        datatype: "json",
        success: function (data) {
            callback(data);
        },
        failure: function () {
            console.log("failure");
        }
    });
}