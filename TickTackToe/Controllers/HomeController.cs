﻿using Core.Services;
using Core.VM;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using TickTackToe.Hubs;

namespace TickTackToe.Controllers
{
    [System.Web.Mvc.Authorize]
    public class HomeController : BaseController
    {
        public IGameService GameService { get; set; }

        public ActionResult Index()
        {
            var id = User.Identity.GetUserId();

            GameService.OnlineGamerOn(id, Request.UserHostAddress,
                 string.Format("browser: {0}; user_agent: {1}", Request.Browser.Browser, Request.UserAgent));

            var vm = GameService.Init(id);
            return View(vm);
        }

        public JsonResult GetUserInfo()
        {
            OnlineGamerVM user = GameService.GetOnlineUserInfo(User.Identity.GetUserId());

            //var myHub = GlobalHost.ConnectionManager.GetHubContext("GameTicTacHub");
            //var result = myHub.Clients.All.TryStandoffAgree("true");
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCommonInfo()
        {
            var id = User.Identity.GetUserId();
            var info = GameService.CommonInfo(id);

            GameService.OnlineGamerOn(id, Request.UserHostAddress,
                string.Format("browser: {0}; user_agent: {1}", Request.Browser.Browser, Request.UserAgent));

            return Json(info, JsonRequestBehavior.AllowGet);
        }


    }
}