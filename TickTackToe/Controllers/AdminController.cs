﻿using Core;
using Core.DB;
using Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TickTackToe.Models;

namespace TickTackToe.Controllers
{
    public class AdminController : BaseController
    {
        public IGameService GameService { get; set; }

        public AdminController()
        { }
        //
        // Get: /Admin/MainPage
        public ActionResult MainPage()
        {
            return View();
        }

        // Get: /Admin/GamersList
        public ActionResult GamersList()
        {
            var gamers = GameService.GetGamersList();
            var gamersVM = new GamersViewModel();
            gamersVM.Gamers = new List<Gamer>();

            foreach (var gamer in gamers)
            {
                AddGamerToList(gamersVM.Gamers, gamer);
            }

            return View("GamersList", gamersVM);
        }
        private void AddGamerToList(List<Gamer> gamers, AspNetUser gamer)
        {
            gamers.Add(new Gamer()
            {
                Name = gamer.Name,
                Age = DateTime.UtcNow.ToLocalTime().Year - gamer.YearOrBirthday,
                UserName = gamer.UserName,
            });
        }
    }
}