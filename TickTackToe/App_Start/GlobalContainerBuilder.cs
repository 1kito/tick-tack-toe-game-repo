﻿using Autofac;
using Core;
using Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TickTackToe.Hubs;

namespace TickTackToe
{
    public abstract class GlobalContainerBuilder
    {
        protected ContainerBuilder builder;

        public GlobalContainerBuilder()
        {
            builder = new ContainerBuilder();

            Reg<TickTackToeDal, IDataAccessLayer>();

            // services
            Reg<GameService, IGameService>();
        }


        public abstract void Reg<TClass>();
        public abstract void Reg<TClass, TInterface>();
        public abstract IContainer Build();

    }
}
