﻿using Autofac;
using Autofac.Integration.Mvc;

namespace TickTackToe
{
    public class WebSiteContainerBuilder : GlobalContainerBuilder
    {
        public override void Reg<TClass>()
        {
            builder.RegisterType<TClass>().PropertiesAutowired().InstancePerHttpRequest();
        }

        public override void Reg<TClass, TInterface>()
        {
            builder.RegisterType<TClass>().As<TInterface>().PropertiesAutowired().InstancePerHttpRequest();
        }

        public override IContainer Build()
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();
            return builder.Build();
        }

        public AutofacDependencyResolver GetDependencyResolver()
        {
            return new AutofacDependencyResolver(Build());
        }
    }
}