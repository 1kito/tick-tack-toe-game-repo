﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLog;

namespace TickTackToe
{
    public class MvcHandleErrorAttribute : HandleErrorAttribute
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            if (filterContext.HttpContext.Request.HttpMethod == "HEAD")
            {
                filterContext.HttpContext.Response.StatusCode = 405;
                filterContext.HttpContext.Response.AddHeader("Allow", "GET");
            }
            else
            {
                // S-187 - не пишем в лог это исключение
                if (filterContext.Exception.Message.Contains("A potentially dangerous Request.Form value was detected from the client")
                    && filterContext.HttpContext.Request.Url.AbsoluteUri.Contains("https://squaretrade.ru/home/pages")
                    )
                {
                    return;
                }

                //filterContext.HttpContext.User.Identity.Name;
                _logger.ErrorException(
                    string.Format(
                        "@{0}; Ошибка поймана в аттрибуте: {1} {2} {3}",
                        filterContext.HttpContext.User.Identity.Name,
                        filterContext.HttpContext.Request.UserHostAddress,
                        filterContext.HttpContext.Request.HttpMethod,
                        filterContext.HttpContext.Request.Url
                    ), filterContext.Exception);
            }
        }
    }
}