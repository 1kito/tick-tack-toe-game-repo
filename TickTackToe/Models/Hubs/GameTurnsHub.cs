﻿using Core.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace TickTackToe.Hubs
{
    [Authorize]
    [HubName("GameTicTacHub")]
    public class GameTurnsHub : Hub<IGameHub>
    {
        public IGameService GameService { get; set; }

        public GameTurnsHub() { }

        public GameTurnsHub(IGameService _gameService)
        {
            GameService = _gameService;
        }


        public void Send(string enterUserID)
        {
            var entered = GameService.GetOnlineUserInfo(enterUserID);
            var ip = Context.Request.Environment["server.RemoteIpAddress"];
            entered.IP = ip != null ? ip.ToString() : "";
            Clients.All.EnterUserHub(entered);
        }

        public void Invite(string inviterUserID, string secondUserID, int fieldSize)
        {
            string userID = Context.User.Identity.GetUserId();
            var result = GameService.TryInvite(userID, inviterUserID, secondUserID, fieldSize);

            Clients.All.TryInvite(result);
        }

        public void Agree(int gameID, string inviterUserID, string inviterName, string secondUserID, string secondUserName, int fieldSize)
        {
            GameService.Agree(gameID, secondUserID);
            Clients.All.Agree(gameID, inviterUserID, inviterName, secondUserID, secondUserName, fieldSize);
        }

        public void Refuse(int gameID, string inviterUserID, string inviterName, string secondUserID, string secondUserName)
        {
            GameService.Refuse(gameID);
            Clients.All.Refuse(gameID, inviterUserID, inviterName, secondUserID, secondUserName);
        }

        public void Turn(int gameID, string authorUserID, int x, int y, bool gameIsStopped, string winnerUserID, string inviterUserID, string secondUserID)
        {
            GameService.Turn(gameID, authorUserID, x, y, gameIsStopped, winnerUserID);
            Clients.All.TryTurn(gameID, authorUserID, x, y, gameIsStopped, winnerUserID, inviterUserID, secondUserID);
        }

        public void Finished(int gameID, string logoffUserID, string winnerUserID)
        {
            var logoffUserName = GameService.GameOff(gameID, logoffUserID);
            Clients.All.TryGameOff(gameID, logoffUserID, logoffUserName, winnerUserID);
        }

        public void StandOff(int gameID, string authorUserID, string authorName)
        {
            Clients.All.TryOfferStandoff(gameID, authorUserID, authorName);
        }

        public void AgreeStandoff(int gameid)
        {
            GameService.GameOff(gameid);
            Clients.All.TryStandoffAgree(gameid);
        }
    }
}
