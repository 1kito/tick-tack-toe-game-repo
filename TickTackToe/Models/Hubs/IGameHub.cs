﻿
using Core.VM;

namespace TickTackToe.Hubs
{
    public interface IGameHub
    {
        void EnterUserHub(OnlineGamerVM userVM);
        void TryInvite(InvitingResultVM gameResultVM);
        void Agree(int gameID, string inviterUserID, string inviterUserName, string secondUserID, string secondUserName, int fieldSize);
        void Refuse(int gameID, string inviterUserID, string inviterUserName, string secondUserID, string secondUserName);
        void TryTurn(int gameid, string authorUserID, int x, int y, bool gameIsStopped, string winnerUserID, string inviterUserID, string secondUserID);
        void TryGameOff(int gameID, string logoffUserID, string logoffUserName, string winnerUserID);

        void TryOfferStandoff(int gameid, string authorUserID, string authorName);
        void TryStandoffAgree(int gameid);
    }
}
