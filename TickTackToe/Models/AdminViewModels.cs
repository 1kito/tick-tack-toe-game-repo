﻿using System.Collections.Generic;

namespace TickTackToe.Models
{
    public class AdminViewModels
    {
    }

    public class GamersViewModel
    {
        public List<Gamer> Gamers { get; set; }
    }

    public class Gamer
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string UserName { get; set; }
    }
}