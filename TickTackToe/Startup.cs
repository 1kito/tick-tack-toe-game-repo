﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TickTackToe.Startup))]
namespace TickTackToe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
