﻿using Core.DB;
using Core.VM;
using System.Collections.Generic;

namespace Core.Services
{
    public interface IGameService
    {
        InitGameVM Init(string userID);

        OnlineGamerVM GetOnlineUserInfo(string userID);

        List<AspNetUser> GetGamersList();

        /// <summary>
        /// Идеально - получаем игроков, с кем играли ранее.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        GameInfoVM CommonInfo(string userID);

        InvitingResultVM TryInvite(string userID, string inviterUserID, string secondUserID, int fieldSize);

        void Agree(int gameID, string secondUserID);
        
        void Refuse(int gameID);

        void OnlineGamerOn(string userID, string IP, string browserInfo);

        void Turn(int gameid, string authorUserID, int x, int y, bool gameIsStopped, string p);

        string GameOff(int gameID, string logoffUserID = "");
    }
}
