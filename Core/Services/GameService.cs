﻿using Core.DB;
using Core.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using NLog;

namespace Core.Services
{
    /// <summary>
    /// Самый главный сервис логики игры
    /// </summary>
    public class GameService : IGameService
    {
        public IDataAccessLayer Db { get; set; }
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();


        public InitGameVM Init(string userID)
        {
            var init = new InitGameVM()
            {
                UserID = userID,
                PopularFieldSize = 5
            };

            var fs = Db.Games
                       .GroupBy(el => el.FieldSize)
                       .OrderByDescending(el => el.Count())
                       .FirstOrDefault();

            if (fs != null && fs.Key > 2)
                init.PopularFieldSize = fs.Key;

            return init;
        }

        public OnlineGamerVM GetOnlineUserInfo(string userID)
        {
            DateTime _from = DateTime.UtcNow.AddMinutes(-2);
            var onliner = Db.OnlineGamers.OrderByDescending(x => x.ID).FirstOrDefault(u => u.UserID == userID && u.Modified > _from);
            return OnlineUserInfo(onliner);
        }

        public List<AspNetUser> GetGamersList()
        {
            return Db.AspNetUsers.ToList();
        }

        public GameInfoVM CommonInfo(string userID)
        {
            DateTime _from = DateTime.UtcNow.AddMonths(-2);
            DateTime _fromOnline = DateTime.UtcNow.AddMinutes(-2);

            var games = Db.Games.Where(x => x.SecondGamerID == userID || x.OnlineGamer.UserID == userID);
            var onliners = Db.OnlineGamers.OrderByDescending(x => x.Modified)
                .Where(x => x.UserID != userID && x.Modified > _from
                    && (games.Any(b => b.SecondGamerID == x.UserID || b.OnlineGamer.UserID == x.UserID)
                    || x.Modified > _fromOnline))
                .ToList().GroupBy(a => a.UserID).Select(g => g.FirstOrDefault()).ToList(); // remove TOLIST

            GameInfoVM game = new GameInfoVM();
            game.OnlineGamers = new List<OnlineGamerVM>();

            foreach (var onliner in onliners)
            {
                OnlineGamerVM gamervm = OnlineUserInfo(onliner);

                game.OnlineGamers.Add(gamervm);
            }

            game.OnlineGamersUserIDs = game.OnlineGamers.Select(i => i.UserID);
            var invites = from u in Db.Games
                          where u.SecondGamerID == userID && !u.Finished.HasValue && u.OnlineGamer.Modified > _fromOnline
                          select u;

            var invitesUnStarted = invites.Where(i => !i.Started.HasValue);
            game.InvitesUnStarted = GetInvitesVM(invitesUnStarted);

            var invitesUnFinished = invites.Where(i => i.Started.HasValue);
            game.InvitesUnFinished = GetInvitesVM(invitesUnFinished);

            game.AllUsersCount = Db.AspNetUsers.Count();
            game.OnlineUsersCount = game.OnlineGamers.Count(x => x.IsOnline);
            game.WinCount = Db.Games.Count(g => g.WinnerID == userID);
            game.LoseCount =
                Db.Games.Count(g => g.WinnerID != userID && (g.SecondGamerID == userID || g.OnlineGamer.UserID == userID));

            return game;
        }

        public void OnlineGamerOn(string userID, string IP, string browserInfo)
        {
            DateTime from = DateTime.UtcNow.AddMinutes(-2);
            OnlineGamer ongamer = Db.OnlineGamers
                .FirstOrDefault(g => g.UserID == userID && g.Modified > from);

            if (ongamer == null)
            {
                ongamer = new OnlineGamer
                {
                    UserID = userID,
                    IP = IP,
                    BrowserInfo = browserInfo,
                    Created = DateTime.UtcNow,
                    Started = DateTime.UtcNow,
                };
                Db.OnlineGamers.Add(ongamer);
            }

            ongamer.Modified = DateTime.UtcNow;
            Db.SaveChanges();
        }

        #region methods invite agree refuse turn gameoff standoff

        public InvitingResultVM TryInvite(string userID, string inviterUserID, string secondUserID, int fieldSize = 3)
        {
            try
            {
                if (userID == inviterUserID && userID != secondUserID)
                {
                    var now = DateTime.UtcNow;

                    DateTime from = now.AddMinutes(-1);
                    DateTime to = now.AddMinutes(1);
                    OnlineGamer ongamer = Db.OnlineGamers.OrderByDescending(x => x.ID).FirstOrDefault(g => g.UserID == secondUserID && g.Modified > from && g.Modified < to);
                    OnlineGamer onlineInviterGamer = Db.OnlineGamers.OrderByDescending(x => x.ID).FirstOrDefault(g => g.UserID == inviterUserID && g.Modified > from && g.Modified < to);
                    if (ongamer == null || onlineInviterGamer == null)
                    {
                        return new InvitingResultVM
                        {
                            IsInvite = false,
                            Error = "К сожалению игрок вышел"
                        };
                    }

                    var game = Db.Games.FirstOrDefault(x => x.SecondGamerID == secondUserID && x.OnlineGamer.UserID == userID && !x.Finished.HasValue && x.Enabled == false);
                    if (game == null)
                    {
                        game = new Game
                        {
                            OnlineGamerID = onlineInviterGamer.ID,
                            SecondGamerID = secondUserID,
                            WhoseTurnID = secondUserID,
                            Created = now,
                            Modified = now,
                            Enabled = false,
                            FieldSize = fieldSize,
                            GUID = Guid.NewGuid().ToString(),
                            PartGUID = Guid.NewGuid().ToString(),
                        };
                        Db.Games.Add(game);
                    }

                    game.Modified = now;
                    Db.SaveChanges();

                    var gamevm = GetGameVM(game);
                    _logger.Info("Пользователь: {0} пригласил: {1}", userID, secondUserID);
                    return new InvitingResultVM
                    {
                        IsInvite = true,
                        Game = gamevm,
                        InviterUserID = game.OnlineGamer.UserID,
                        InviterName = game.OnlineGamer.AspNetUser.Name
                    };
                }

                return new InvitingResultVM
                {
                    IsInvite = false,
                    Error = "Не для вас"
                };

            }
            catch (Exception e)
            {
                return new InvitingResultVM
                {
                    IsInvite = false,
                    Error = e.Message
                };
            }
        }

        public void Agree(int gameID, string secondUserID)
        {
            var game = Db.Games.FirstOrDefault(x => x.ID == gameID);
            if (game != null)
            {
                game.Started = DateTime.UtcNow;
                game.Modified = DateTime.UtcNow;
                game.Enabled = true;
                game.WhoseTurnID = secondUserID;
                Db.SaveChanges();
            }
        }

        public void Refuse(int gameID)
        {
            var game = Db.Games.FirstOrDefault(x => x.ID == gameID);
            if (game != null)
            {
                game.Modified = DateTime.UtcNow;
                game.Finished = DateTime.UtcNow;
                game.Enabled = false;

                Db.SaveChanges();
            }
        }

        public void Turn(int gameid, string authorUserID, int x, int y, bool gameIsStopped, string winnerUserID)
        {
            var turn = Db.GameTurns.SingleOrDefault(t => t.GameID == gameid && t.Horizontal == x && t.Vertical == y);

            if (turn == null)
            {
                Db.GameTurns.Add(new GameTurn()
                {
                    Created = DateTime.UtcNow,
                    Modified = DateTime.UtcNow,
                    GameID = gameid,
                    Horizontal = x,
                    Vertical = y,
                    UserID = authorUserID,
                });
            }
            else
            {
                turn.Modified = DateTime.UtcNow;
            }

            var game = Db.Games.Single(g => g.ID == gameid);
            game.Modified = DateTime.UtcNow;
            if (gameIsStopped)
            {
                game.Enabled = false;

                game.Finished = DateTime.UtcNow;
                game.WinnerID = winnerUserID;
                game.Enabled = false;
            }

            Db.SaveChanges();
        }

        public string GameOff(int gameID, string logoffUserID = "")
        {
            var game = Db.Games.FirstOrDefault(x => x.ID == gameID);
            if (game != null)
            {
                if (!string.IsNullOrWhiteSpace(logoffUserID))
                {
                    if (game.SecondGamerID == logoffUserID)
                    {
                        game.WinnerID = game.OnlineGamer.UserID;
                    }
                    else
                    {
                        game.WinnerID = game.SecondGamerID;
                    }
                }
                game.Modified = DateTime.UtcNow;
                game.Finished = DateTime.UtcNow;
                game.Enabled = false;

                Db.SaveChanges();
            }
            return Db.AspNetUsers.Single(u => u.Id == logoffUserID).Name;
        }

        #endregion


        #region private methods

        private OnlineGamerVM OnlineUserInfo(OnlineGamer onliner)
        {
            OnlineGamerVM gamervm = new OnlineGamerVM();
            Mapper.Map(onliner, gamervm);
            var user = onliner.AspNetUser;
            gamervm.UserID = user.Id;
            gamervm.UserName = user.UserName;
            gamervm.Created = onliner.Created.ToLocalTime();
            gamervm.Started = onliner.Started.HasValue ? onliner.Started.Value.ToLocalTime() : gamervm.Created;
            gamervm.Modified = onliner.Modified.ToLocalTime();
            var gameLasted = Db.Games.OrderByDescending(g => g.Created).FirstOrDefault(g => g.SecondGamerID == onliner.UserID || g.OnlineGamer.UserID == onliner.UserID);
            gamervm.Lasted = gameLasted != null ? gameLasted.Modified.ToLocalTime().ToShortDateString() : string.Empty;

            gamervm.WinCount = Db.Games.Count(g => g.WinnerID == onliner.UserID);
            gamervm.LoseCount = Db.Games.Count(g => g.WinnerID != onliner.UserID && (g.SecondGamerID == onliner.UserID || g.OnlineGamer.UserID == onliner.UserID));
            DateTime _from = DateTime.UtcNow.AddMinutes(-1);
            gamervm.IsOnline = onliner.Modified > _from;
            gamervm.IsGamed = false;
            if (gamervm.IsOnline)
            {
                var currGame = Db.Games.OrderByDescending(g => g.ID).FirstOrDefault(g => !g.Finished.HasValue && g.Started.HasValue && g.Modified > g.Created && !g.Deleted);
                gamervm.IsGamed = currGame != null;
            }
            gamervm.Name = user.Name;
            gamervm.Age = DateTime.UtcNow.ToLocalTime().Year - user.YearOrBirthday;
            gamervm.RegTime = user.RegTime.ToLocalTime().ToShortDateString();
            gamervm.RegisterTime = user.RegTime.ToLocalTime().ToString();
            return gamervm;
        }

        private List<GameVM> GetInvitesVM(IQueryable<Game> invites)
        {
            var invitesvm = new List<GameVM>();
            foreach (var game in invites)
            {
                var gameVM = GetGameVM(game);
                invitesvm.Add(gameVM);
            }

            return invitesvm;
        }

        private GameVM GetGameVM(Game g)
        {
            var gamevm = new GameVM();

            Mapper.Map(g, gamevm);
            gamevm.InviterName = g.OnlineGamer.AspNetUser.Name;
            gamevm.InviterUserID = g.OnlineGamer.AspNetUser.Id;
            gamevm.SecondUserName = Db.AspNetUsers.SingleOrDefault(u => u.Id == g.SecondGamerID).Name;
            gamevm.SecondUserID = Db.AspNetUsers.SingleOrDefault(u => u.Id == g.SecondGamerID).Id;

            return gamevm;
        }

        #endregion
    }
}
