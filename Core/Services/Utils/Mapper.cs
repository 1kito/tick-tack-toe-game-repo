﻿using System.Linq;

namespace Core
{
    /// <summary>
    /// Ма всех полей рефлексивно
    /// </summary>
    public static class Mapper
    {
        public static void Map<X, Y>(X CopyFrom, Y CopyTo, string Exclude = "")
        {
            var exclude = (from e in Exclude.Split(',')
                           where !string.IsNullOrEmpty(e.Trim())
                           select e.Trim()).ToList();

            var x_props = typeof(X).GetProperties();

            foreach (var x_prop in x_props)
            {
                if (!exclude.Contains(x_prop.Name))
                {
                    var y_prop = typeof(Y).GetProperty(x_prop.Name);

                    if (y_prop != null)
                    {
                        typeof(Y).GetProperty(x_prop.Name).SetValue(CopyTo, x_prop.GetValue(CopyFrom, null), null);
                    }
                }
            }
        }
    }
}