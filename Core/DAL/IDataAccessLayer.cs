﻿using Core.DB;
using System;

namespace Core
{
    public interface IDataAccessLayer : IDisposable
    {
        IRepository<AspNetUser> AspNetUsers { get; }
        IRepository<AppSetting> AppSettings { get; }
        IRepository<Game> Games { get; }
        IRepository<OnlineGamer> OnlineGamers { get; }
        IRepository<GameTurn> GameTurns { get; }
        void SaveChanges();
        void Detach(object entity);
    }
}
