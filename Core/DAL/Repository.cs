﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
//using Core.Dto;

namespace Core
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        protected DbSet<T> _set; 

        public Repository(DbSet<T> set)
        {
            _set = set;
        }

        public T Single(int id)
        {
            var entity = _set.Single(t => t.ID == id);

            if (entity is IEntityDeletable)
                if (((IEntityDeletable)entity).Deleted)
                    return null;

            return entity;
        }

        public T SingleOrDefault(int id)
        {
            var entity = _set.SingleOrDefault(t => t.ID == id);

            if (entity is IEntityDeletable)
                if (((IEntityDeletable)entity).Deleted)
                    return null;

            return entity;
        }

        public void Add(T newEntity)
        {
            _set.Add(newEntity);
        }

        public void Remove(T entity)
        {
            _set.Remove(entity);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _set.AsQueryable<T>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Type ElementType
        {
            get { return _set.AsQueryable<T>().ElementType; }
        }

        public Expression Expression
        {
            get { return _set.AsQueryable<T>().Expression; }
        }

        public IQueryProvider Provider
        {
            get { return _set.AsQueryable<T>().Provider; }
        }
    }
}
