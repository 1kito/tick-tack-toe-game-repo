﻿using System;
using System.Linq;

namespace Core
{
    public interface IRepository<T> : IQueryable<T> where T : class, IEntity
    {
        T Single(int id);

        T SingleOrDefault(int id);

        void Add(T newEntity);

        void Remove(T entity);
    }
}
