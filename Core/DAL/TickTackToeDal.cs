﻿using Core.DB;
using NLog;
using System;
using System.Data.Entity.Infrastructure;


namespace Core
{
    public class TickTackToeDal : IDataAccessLayer
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private TickTackToeContext _db;

        public TickTackToeDal()
        {
            _db = new TickTackToeContext();
        }

        #region main implement interface

        public void SaveChanges()
        {
            try
            {
                _db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        _logger.Error(message, dbEx);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public void Dispose()
        {
            if (_db != null)
            {
                _db.Dispose();
            }
        }

        public void Detach(object entity)
        {
            (_db as IObjectContextAdapter).ObjectContext.Detach(entity);
        }

        #endregion


        public IRepository<AspNetUser> AspNetUsers
        {
            get { return new Repository<AspNetUser>(_db.AspNetUsers); }
        }

        public IRepository<AppSetting> AppSettings
        {
            get { return new Repository<AppSetting>(_db.AppSettings); }
        }

        public IRepository<Game> Games
        {
            get { return new Repository<Game>(_db.Games); }
        }

        public IRepository<OnlineGamer> OnlineGamers
        {
            get { return new Repository<OnlineGamer>(_db.OnlineGamers); }
        }

        public IRepository<GameTurn> GameTurns
        {
            get { return new Repository<GameTurn>(_db.GameTurns); }
        }
    }
}
