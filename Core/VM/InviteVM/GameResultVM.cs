﻿
namespace Core.VM
{
    public class InvitingResultVM
    {
        public bool IsInvite { get; set; }

        public string Error { get; set; }

        public GameVM Game { get; set; }

        public string InviterUserID { get; set; }
        public string InviterName { get; set; }
    }
}
