﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.VM
{
    public class GameVM
    {
        public int ID { get; set; }
        public int OnlineGamerID { get; set; }
        public string GUID { get; set; }
        public string PartGUID { get; set; }
        public string WhoseTurnID { get; set; }
        public string SecondGamerID { get; set; }
        public string WinnerID { get; set; }
        public int FieldSize { get; set; }
        public int WinSize { get; set; }
        public bool Enabled { get; set; }
        public Nullable<System.DateTime> Started { get; set; }
        public Nullable<System.DateTime> Finished { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime Modified { get; set; }
        public bool Deleted { get; set; }

        public string InviterName { get; set; }
        public string InviterUserID { get; set; }
        public string SecondUserName { get; set; }
        public string SecondUserID { get; set; }
    }
}
