﻿using System;

namespace Core.VM
{
    public class OnlineGamerVM
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public Nullable<int> GameID { get; set; }
        public string IP { get; set; }
        public string BrowserInfo { get; set; }

        /// <summary>
        /// Дата и время старта или время создания
        /// </summary>
        public Nullable<DateTime> Started { get; set; }
        public Nullable<DateTime> Finished { get; set; }
        public DateTime Created { get; set; }

        /// <summary>
        /// Дата и время изменений
        /// </summary>
        public DateTime Modified { get; set; }
        public bool Deleted { get; set; }

        public string UserName { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public string RegTime { get; set; }

        /// <summary>
        /// Количество победителей в режиме online
        /// </summary>
        public int WinCount { get; set; }

        /// <summary>
        /// Количество победителей не в режиме online, которые....
        /// </summary>
        public int LoseCount { get; set; }

        /// <summary>
        /// Возраст игрока
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Дата и время регистрации
        /// </summary>
        public string RegisterTime { get; set; }

        /// <summary>
        /// Имя игрока
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// последняя дата присутствия на сайте Online
        /// </summary>
        public string Lasted { get; set; }

        /// <summary>
        /// находится ли игрок в online
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// играет ли сейчас с кем-нибудь игрок
        /// </summary>
        public bool IsGamed { get; set; }
    }
}
