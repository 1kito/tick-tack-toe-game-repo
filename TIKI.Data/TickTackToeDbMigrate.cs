﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;
using System;

namespace TIKI.Data
{
    public class TickTackToeDbMigrate
    {
        public static void Migrate(IHost host, Logger logger)
        {
            using var scope = host.Services.CreateScope();

            var services = scope.ServiceProvider;

            try
            {
                var context = services.GetService<TickTackToeContext>();
                context.Database.Migrate();
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e, "Migration cannot be created!");
                throw;
            }
        }
    }
}
