﻿using TIKI.Core.Repositories;
using TIKI.Data.Models;
using TIKI.Data.Repositories.ExtendedModels;
using TIKI.Data.Repositories.RepositoryModels;
using TIKI.Data.RepositoryInterfaces;

namespace TIKI.Data.Repositories
{
    public class AspNetUserRepository 
        : BaseRepository<AspNetUser, string, AspNetUserModel, ExtendedAspNetUserModel, TickTackToeContext>, 
        IAspNetUserRepository
    {
        public AspNetUserRepository(TickTackToeContext context)
            : base(context)
        {
        }
    }
}
