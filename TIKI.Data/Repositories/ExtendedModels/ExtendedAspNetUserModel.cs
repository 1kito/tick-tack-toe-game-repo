﻿using System.Linq;
using TIKI.Core.Repositories.Models;
using TIKI.Data.Models;
using TIKI.Data.Repositories.RepositoryModels;

namespace TIKI.Data.Repositories.ExtendedModels
{
    public class ExtendedAspNetUserModel : AspNetUserModel, IExtendedBaseModel<AspNetUser, string, TickTackToeContext>
    {
        /// <summary>
        /// Количество строк, которое нужно получить
        /// </summary>
        public int TakeCount { get; set; } = int.MaxValue;

        /// <summary>
        /// Количество строк, которое нужно пропустить
        /// </summary>
        public int SkipCount { get; set; } = 0;

        /// <summary>
        /// Поле для сортировки
        /// </summary>
        public string Ordering { get; set; }

        /// <summary>
        /// Порядок сортировки по возрастанию? По умолчанию "true".
        /// </summary>
        public bool Ascending { get; set; } = true;

        public override IQueryable<AspNetUser> GetQueryable(TickTackToeContext context)
        {
            var query = base.GetQueryable(context);
            query = this.AddSkipCount(query);
            query = this.AddTakeCount(query);

            if (!string.IsNullOrWhiteSpace(Ordering))
            {
                query = this.AddOrder(query);
            }

            return query;
        }
    }
}
