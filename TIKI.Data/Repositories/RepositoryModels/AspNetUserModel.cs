﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIKI.Core.Repositories.Models;
using TIKI.Data.Models;

namespace TIKI.Data.Repositories.RepositoryModels
{
    public class AspNetUserModel : BaseModel<AspNetUser, string, TickTackToeContext>
    {
        public AspNetUserModel()
        {
            //Email = email;
            //CreatedDate = createdDate;
        }

        public string Email { get; set; }

        public DateTime? CreatedDate { get; set; }

        //public static StatusEmailModel Create(string email)
        //{
        //    return new StatusEmailModel { Email = email };
        //}

        public override IQueryable<AspNetUser> GetQueryable(TickTackToeContext context)
        {
            var query = base.GetQueryable(context);

            if (!string.IsNullOrEmpty(Email))
            {
                query = query.Where(l => l.Email == Email);
            }

            if (CreatedDate.HasValue)
            {
                // query = query.Where(l => l.CreatedAt.Date == CreatedDate.Value.Date);
            }

            return query;
        }
    }
}
