﻿using System;
using System.Collections.Generic;
using System.Text;
using TIKI.Core.Interfaces;
using TIKI.Data.Models;
using TIKI.Data.Repositories.ExtendedModels;
using TIKI.Data.Repositories.RepositoryModels;

namespace TIKI.Data.RepositoryInterfaces
{
    public interface IAspNetUserRepository : IBaseRepository<
            AspNetUser,
            string,
            AspNetUserModel,
            ExtendedAspNetUserModel,
            TickTackToeContext>
    {
    }
}
