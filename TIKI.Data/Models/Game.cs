﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TIKI.Data.Models
{
    public partial class Game
    {
        public Game()
        {
            GameTurns = new HashSet<GameTurn>();
        }

        public int Id { get; set; }
        public int OnlineGamerId { get; set; }
        public string Guid { get; set; }
        public string PartGuid { get; set; }
        public string WhoseTurnId { get; set; }
        public string SecondGamerId { get; set; }
        public string WinnerId { get; set; }
        public int FieldSize { get; set; }
        public int WinSize { get; set; }
        public bool Enabled { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? Finished { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Deleted { get; set; }

        public virtual OnlineGamer OnlineGamer { get; set; }
        public virtual ICollection<GameTurn> GameTurns { get; set; }
    }
}
