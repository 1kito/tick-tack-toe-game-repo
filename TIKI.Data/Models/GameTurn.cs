﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TIKI.Data.Models
{
    public partial class GameTurn
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public string UserId { get; set; }
        public int Vertical { get; set; }
        public int Horizontal { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Deleted { get; set; }

        public virtual Game Game { get; set; }
        public virtual AspNetUser User { get; set; }
    }
}
