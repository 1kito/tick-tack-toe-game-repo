﻿using System;
using System.Collections.Generic;
using TIKI.Core.Entities;

#nullable disable

namespace TIKI.Data.Models
{
    public partial class AspNetUser : EntityBase<string>
    {
        public AspNetUser()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaim>();
            AspNetUserLogins = new HashSet<AspNetUserLogin>();
            AspNetUserRoles = new HashSet<AspNetUserRole>();
            GameTurns = new HashSet<GameTurn>();
            OnlineGamers = new HashSet<OnlineGamer>();
        }

        // public string Id { get; set; }
        public string Name { get; set; }
        public int YearOrBirthday { get; set; }
        public DateTime RegTime { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual ICollection<AspNetUserRole> AspNetUserRoles { get; set; }
        public virtual ICollection<GameTurn> GameTurns { get; set; }
        public virtual ICollection<OnlineGamer> OnlineGamers { get; set; }
    }
}
