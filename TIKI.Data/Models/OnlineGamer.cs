﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TIKI.Data.Models
{
    public partial class OnlineGamer
    {
        public OnlineGamer()
        {
            Games = new HashSet<Game>();
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        public string Ip { get; set; }
        public string BrowserInfo { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? Finished { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool Deleted { get; set; }

        public virtual AspNetUser User { get; set; }
        public virtual ICollection<Game> Games { get; set; }
    }
}
