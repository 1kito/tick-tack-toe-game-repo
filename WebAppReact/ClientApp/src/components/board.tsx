﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApplicationState } from '../store';
import * as GamePointsStore from '../store/LoadGame';

// At runtime, Redux will merge together...
type GamePointProps =
    GamePointsStore.GamePointsState // ... state we've requested from the Redux store
    & typeof GamePointsStore.actionCreators // ... plus action creators we've requested
    & RouteComponentProps<{ startDateIndex: string }>; // ... plus incoming routing parameters


class Board extends React.PureComponent<GamePointProps> {
    // This method is called when the component is first added to the document
    public componentDidMount() {
        this.ensureDataFetched();
    }

    // This method is called when the route parameters change
    public componentDidUpdate() {
        this.ensureDataFetched();
    }

    public render() {
        return (
            <React.Fragment>
                <h1 id="tabelLabel">Game points</h1>
                <p>This component demonstrates fetching data from the server and working with URL parameters.</p>
                {this.renderGamePointsTable()}
                {this.renderPagination()}
            </React.Fragment>
        );
    }

    private ensureDataFetched() {
        const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
        this.props.requestGamePoints(startDateIndex);
    }

    private renderGamePointsTable() {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Id. (Id)</th>
                        <th>Temp. (C)</th>
                        <th>Temp. (F)</th>
                        <th>Summary {this.props.gamepoints.join(",")}</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.gamepoints.map((pt: GamePointsStore.GamePoint) =>
                        <tr key={pt.id}>
                            <td>{pt.id}</td>
                            <td>{pt.x}</td>
                            <td>{pt.y}</td>
                            <td>{pt.isX}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    private renderPagination() {
        const prevStartDateIndex = (this.props.startDateIndex || 0) - 5;
        const nextStartDateIndex = (this.props.startDateIndex || 0) + 5;

        return (
            <div className="d-flex justify-content-between">
                <Link className='btn btn-outline-secondary btn-sm' to={`/start-game/${prevStartDateIndex}`}>Previous</Link>
                {this.props.isLoading && <span>Loading...</span>}
                <Link className='btn btn-outline-secondary btn-sm' to={`/start-game/${nextStartDateIndex}`}>Next</Link>
            </div>
        );
    }
}

export default connect(
    (state: ApplicationState) => state.gamePoints, // Selects which state properties are merged into the component's props
    GamePointsStore.actionCreators // Selects which action creators are merged into the component's props
)(Board as any);
