import * as WeatherForecasts from './WeatherForecasts';
import * as Counter from './Counter';
import * as GamePoints from './LoadGame';

//const DEFAULT_PORT = 5053;
//const CORE_SETTINGS = {
//    PORT: process.env.PORT || DEFAULT_PORT,
//    NODE_ENV: process.env.NODE_ENV || 'development',
//    API_URL: process.env.API_URL || 'http://localhost:5053/api'
//}

// The top-level state object
export interface ApplicationState {
    counter: Counter.CounterState | undefined;
    weatherForecasts: WeatherForecasts.WeatherForecastsState | undefined;
    gamePoints: GamePoints.GamePointsState | undefined;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    counter: Counter.reducer,
    weatherForecasts: WeatherForecasts.reducer,
    gamePoints: GamePoints.reducer
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface AppThunkAction<TAction> {
    (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}
