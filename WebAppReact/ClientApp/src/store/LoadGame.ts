﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

const API_URL_ORIGIN = process.env.NODE_ENV !== 'development' ? 'http://tiki.1gb.ru' : 'http://localhost:45882';
const API_URL = process.env.NODE_ENV !== 'development' ? 'http://tiki-api.1gb.ru' : 'http://localhost:5053';
const LOAD_GAME_WEB_API_URL = API_URL + `/api/game/loadGame`;

// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface GamePointsState {
    isLoading: boolean;
    startDateIndex?: number;
    gamepoints: GamePoint[];
}

export interface GamePoint {
    id: string;
    x: number;
    y: number;
    isX: boolean;
}

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.

interface RequestGamePointsAction {
    type: 'REQUEST_GAME_POINTS';
    startDateIndex: number;
}

interface ReceiveGamePointsAction {
    type: 'RECEIVE_GAME_POINTS';
    startDateIndex: number;
    gamepoints: GamePoint[];
}

// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction = RequestGamePointsAction | ReceiveGamePointsAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    requestGamePoints: (startDateIndex: number): AppThunkAction<KnownAction> => (dispatch, getState) => {
        // console.log('API_URL_ORIGIN', API_URL_ORIGIN);
        // console.log('WEB_API_URL', WEB_API_URL);
        // Only load data if it's something we don't already have (and are not already loading)
        const appState = getState();

        const param = {
            method: 'GET',
            // mode: 'no-cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': API_URL_ORIGIN,
                // 'Access-Control-Allow-Credentials': 'true',
                "Access-Control-Request-Method": "GET",
                "access-control-allow-methods": "GET,OPTIONS,POST",
                "Origin": API_URL_ORIGIN,
                "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin, access-control-allow-methods",
                'Access-Control-Request-Headers': "Origin, Accept, Content-Type, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods",
            },
        };

        if (appState && appState.gamePoints && startDateIndex !== appState.gamePoints.startDateIndex) {
            fetch(LOAD_GAME_WEB_API_URL)
                .then(response => response.json() as Promise<GamePoint[]>)
                .then(data => {
                    dispatch({ type: 'RECEIVE_GAME_POINTS', startDateIndex: startDateIndex, gamepoints: data });
                });

            dispatch({ type: 'REQUEST_GAME_POINTS', startDateIndex: startDateIndex });
        }
    }
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

const unloadedState: GamePointsState = { gamepoints: [], isLoading: false };

export const reducer: Reducer<GamePointsState> = (state: GamePointsState | undefined, incomingAction: Action): GamePointsState => {
    if (state === undefined) {
        return unloadedState;
    }

    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_GAME_POINTS':
            return {
                startDateIndex: action.startDateIndex,
                gamepoints: state.gamepoints,
                isLoading: true
            };
        case 'RECEIVE_GAME_POINTS':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startDateIndex === state.startDateIndex) {
                return {
                    startDateIndex: action.startDateIndex,
                    gamepoints: action.gamepoints,
                    isLoading: false
                };
            }
            break;
    }

    return state;
};
