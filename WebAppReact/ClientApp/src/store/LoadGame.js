"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reducer = exports.actionCreators = void 0;
var API_URL_ORIGIN = process.env.NODE_ENV !== 'development' ? 'http://tiki.1gb.ru' : 'http://localhost:45882';
var API_URL = process.env.NODE_ENV !== 'development' ? 'http://tiki-api.1gb.ru' : 'http://localhost:5053';
var LOAD_GAME_WEB_API_URL = API_URL + "/api/game/loadGame";
// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).
exports.actionCreators = {
    requestGamePoints: function (startDateIndex) { return function (dispatch, getState) {
        // console.log('API_URL_ORIGIN', API_URL_ORIGIN);
        // console.log('WEB_API_URL', WEB_API_URL);
        // Only load data if it's something we don't already have (and are not already loading)
        var appState = getState();
        var param = {
            method: 'GET',
            // mode: 'no-cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': API_URL_ORIGIN,
                // 'Access-Control-Allow-Credentials': 'true',
                "Access-Control-Request-Method": "GET",
                "access-control-allow-methods": "GET,OPTIONS,POST",
                "Origin": API_URL_ORIGIN,
                "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin, access-control-allow-methods",
                'Access-Control-Request-Headers': "Origin, Accept, Content-Type, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods",
            },
        };
        if (appState && appState.gamePoints && startDateIndex !== appState.gamePoints.startDateIndex) {
            fetch(LOAD_GAME_WEB_API_URL)
                .then(function (response) { return response.json(); })
                .then(function (data) {
                dispatch({ type: 'RECEIVE_GAME_POINTS', startDateIndex: startDateIndex, gamepoints: data });
            });
            dispatch({ type: 'REQUEST_GAME_POINTS', startDateIndex: startDateIndex });
        }
    }; }
};
// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.
var unloadedState = { gamepoints: [], isLoading: false };
var reducer = function (state, incomingAction) {
    if (state === undefined) {
        return unloadedState;
    }
    var action = incomingAction;
    switch (action.type) {
        case 'REQUEST_GAME_POINTS':
            return {
                startDateIndex: action.startDateIndex,
                gamepoints: state.gamepoints,
                isLoading: true
            };
        case 'RECEIVE_GAME_POINTS':
            // Only accept the incoming data if it matches the most recent request. This ensures we correctly
            // handle out-of-order responses.
            if (action.startDateIndex === state.startDateIndex) {
                return {
                    startDateIndex: action.startDateIndex,
                    gamepoints: action.gamepoints,
                    isLoading: false
                };
            }
            break;
    }
    return state;
};
exports.reducer = reducer;
//# sourceMappingURL=LoadGame.js.map