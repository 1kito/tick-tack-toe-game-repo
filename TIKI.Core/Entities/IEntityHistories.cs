﻿namespace TIKI.Core.Entities
{
    public interface IEntityHistories<TMainEntity, TKey> : IEntityBase<TKey>
        where TMainEntity : IEntityBase<TKey>, new()
    {
        /// <summary>
        /// Код сущности на которую делается история
        /// </summary>
        TKey MainEntityId { get; set; }

        /// <summary>
        /// Число обозначающие номер изменения в истории данной записи
        /// </summary>
        public int OptimisticLock { get; set; }
    }
}