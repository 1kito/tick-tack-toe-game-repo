﻿namespace TIKI.Core.Entities
{
    public interface IEntityWithHistories
    {
        /// <summary>
        /// Число обозначающие номер изменения в истории данной записи
        /// </summary>
        public int OptimisticLock { get; set; }
    }
}
