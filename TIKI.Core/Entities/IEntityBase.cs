﻿using System;

namespace TIKI.Core.Entities
{
    public interface IEntityBase<TKey>
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        public TKey Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime UpdatedAt { get; set; }
    }
}