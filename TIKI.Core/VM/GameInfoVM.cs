﻿using System.Collections.Generic;

namespace TIKI.Core.VM
{
    public class GameInfoVM
    {
        /// <summary>
        /// Поле нигде не используется
        /// </summary>
        public UserVM User { get; set; }

        /// <summary>
        /// Список игроков в режиме online
        /// </summary>
        public List<OnlineGamerVM> OnlineGamers { get; set; }


        /// <summary>
        /// ID игроков находящихся в режиме online
        /// </summary>
        public IEnumerable<string> OnlineGamersUserIDs { get; set; }

        public List<GameVM> InvitesUnStarted { get; set; }

        public List<GameVM> InvitesUnFinished { get; set; }

        /// <summary>
        /// Количество всех игроков
        /// </summary>
        public int AllUsersCount { get; set; }

        /// <summary>
        /// Количество выигранных игр
        /// </summary>
        public int WinCount { get; set; }

        /// <summary>
        /// Количество проигранных игр
        /// </summary>
        public int LoseCount { get; set; }

        /// <summary>
        /// Количество игроков в режиме online
        /// </summary>
        public int OnlineUsersCount { get; set; }
    }
}
