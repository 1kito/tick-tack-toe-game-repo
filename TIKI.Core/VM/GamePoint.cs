﻿namespace TIKI.Core.VM
{
    public class GamePoint
    {
        public string Id { get; set; }

        public int X { get; set; }
        
        public int Y { get; set; }

        public bool IsX { get; set; }
    }
}
