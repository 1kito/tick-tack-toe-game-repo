﻿namespace TIKI.Core.VM
{
    public class SheetVM
    {
        public int Size { get; set; }
        public GamePoint[] Points { get; set; }
    }
}
