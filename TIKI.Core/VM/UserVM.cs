﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIKI.Core.VM
{
    public class UserVM
    {
        public string Id { get; set; }

        /// <summary>
        /// Имя при регистрации
        /// </summary>
        public string Name { get; set; }

        public int Age { get; set; }
        public string Email { get; set; }

        /// <summary>
        /// Почта при регистрации
        /// </summary>
        public string UserName { get; set; }
        public string RegShortTime { get; set; }

        public string IP { get; set; }

        public int WinCount { get; set; }

        public int LoseCount { get; set; }

        public string RegTime { get; set; }
    }
}
