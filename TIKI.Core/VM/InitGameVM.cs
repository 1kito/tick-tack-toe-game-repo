﻿
namespace TIKI.Core.VM
{
    public class InitGameVM
    {
        public string UserID { get; set; }
        public int PopularFieldSize { get; set; }

        public SheetVM SheetGame { get; set; }
    }
}
