﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TIKI.Core.Factories;

namespace TIKI.Core.Helpers
{
    public class TransactionHelper
    {
        public static async Task InvokeWithEmbeddedTransaction<TContext>(Func<Task> method, ITransactionFactory<TContext> transactionFactory)
            where TContext : DbContext
        {
            if (transactionFactory.IsExistCurrentTransaction)
            {
                await method.Invoke();
            }
            else
            {
                await using var transaction = await transactionFactory.OpenTransactionAsync();
                await method.Invoke();
                await transaction.CommitAsync();
            }
        }
    }
}
