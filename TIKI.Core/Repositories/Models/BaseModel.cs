﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TIKI.Core.Entities;

namespace TIKI.Core.Repositories.Models
{
    public abstract class BaseModel<TEntity, TKey, TContext>
        : IBaseModel<TEntity, TKey, TContext>
        where TContext : DbContext
        where TEntity : EntityBase<TKey>, new()
    {
        /// <summary>
        /// Список идентификаторов сущностей
        /// </summary>
        public TKey[] Ids { get; set; }

        /// <summary>
        /// Дата создания от
        /// </summary>
        /// <remarks>Граница задается включительно</remarks>
        public DateTime? CreatedAtFrom { get; set; }

        /// <summary>
        /// Дата создания до
        /// </summary>
        /// <remarks>Граница задается НЕ включительно</remarks>
        public DateTime? CreatedAtTo { get; set; }

        /// <summary>
        /// Дата изменения от
        /// </summary>
        /// <remarks>Граница задается включительно</remarks>
        public DateTime? UpdatedAtFrom { get; set; }

        /// <summary>
        /// Дата изменения до
        /// </summary>
        /// <remarks>Граница задается НЕ включительно</remarks>
        public DateTime? UpdatedAtTo { get; set; }

        /// <inheritdoc />
        public virtual IQueryable<TEntity> GetQueryable(TContext context)
        {
            var query = (IQueryable<TEntity>)context.Set<TEntity>();

            if (!(this is IExtendedBaseModel<TEntity, TKey, TContext>))
            {
                query = query.OrderBy(q => q.Id);
            }

            if (Ids?.Length > 0)
            {
                if (Ids.Length == 1)
                {
                    var id = Ids[0];
                    query = query.Where(m => Equals(m.Id, id));
                }
                else
                {
                    query = query.Where(m => Ids.Contains(m.Id));
                }
            }

            if (CreatedAtFrom.HasValue)
            {
                var createdAtFrom = CreatedAtFrom.Value;
                query = query.Where(m => m.CreatedAt >= createdAtFrom);
            }

            if (CreatedAtTo.HasValue)
            {
                var createdAtTo = CreatedAtTo.Value;
                query = query.Where(m => m.CreatedAt < createdAtTo);
            }

            if (UpdatedAtFrom.HasValue)
            {
                var updatedAtFrom = UpdatedAtFrom.Value;
                query = query.Where(m => m.UpdatedAt >= updatedAtFrom);
            }

            if (UpdatedAtTo.HasValue)
            {
                var updatedAtTo = UpdatedAtTo.Value;
                query = query.Where(m => m.UpdatedAt < updatedAtTo);
            }

            return query;
        }
    }
}
