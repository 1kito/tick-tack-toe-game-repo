﻿using Microsoft.EntityFrameworkCore;
using TIKI.Core.Entities;

namespace TIKI.Core.Repositories.Models
{
    public interface IExtendedBaseModel<TEntity, TKey, TContext>
        : IBaseModel<TEntity, TKey, TContext>
        where TContext : DbContext
        where TEntity : IEntityBase<TKey>, new()
    {
        /// <summary>
        /// Количество строк, которое нужно получить
        /// </summary>
        int TakeCount { get; set; }

        /// <summary>
        /// Количество строк, которое нужно пропустить
        /// </summary>
        int SkipCount { get; set; }

        /// <summary>
        /// Поле для сортировки
        /// </summary>
        string Ordering { get; set; }

        /// <summary>
        /// Порядок сортировки по возрастанию? По умолчанию "true".
        /// </summary>
        bool Ascending { get; set; }
    }
}
