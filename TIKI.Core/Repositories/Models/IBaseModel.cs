﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TIKI.Core.Entities;

namespace TIKI.Core.Repositories.Models
{
    public interface IBaseModel<TEntity, TKey, TContext>
        where TContext : DbContext
        where TEntity : IEntityBase<TKey>, new()
    {
        /// <summary>
        /// Список идентификаторов сущностей
        /// </summary>
        TKey[] Ids { get; set; }

        /// <summary>
        /// Дата создания от
        /// </summary>
        /// <remarks>Граница задается включительно</remarks>
        DateTime? CreatedAtFrom { get; set; }

        /// <summary>
        /// Дата создания до
        /// </summary>
        /// <remarks>Граница задается НЕ включительно</remarks>
        DateTime? CreatedAtTo { get; set; }

        /// <summary>
        /// Дата изменения от
        /// </summary>
        /// <remarks>Граница задается включительно</remarks>
        DateTime? UpdatedAtFrom { get; set; }

        /// <summary>
        /// Дата изменения до
        /// </summary>
        /// <remarks>Граница задается НЕ включительно</remarks>
        DateTime? UpdatedAtTo { get; set; }

        /// <summary>
        /// Получить запрос с фильтрацией.
        /// </summary>
        /// <param name="context">Контекст БД.</param>
        /// <returns>Запрос.</returns>
        IQueryable<TEntity> GetQueryable(TContext context);
    }
}
