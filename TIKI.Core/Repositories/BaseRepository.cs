﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TIKI.Core.Entities;
using TIKI.Core.Interfaces;
using TIKI.Core.Repositories.Models;

namespace TIKI.Core.Repositories
{
    public abstract class BaseRepository<TEntity, TKey, TModel, TExtendedModel, TContext>
        : IBaseRepository<TEntity, TKey, TModel, TExtendedModel, TContext>
        where TContext : DbContext
        where TEntity : EntityBase<TKey>, new()
        where TModel : IBaseModel<TEntity, TKey, TContext>, new()
        where TExtendedModel : IExtendedBaseModel<TEntity, TKey, TContext>, new()
    {
        protected BaseRepository(TContext context)
        {
            Context = context ?? throw new NullReferenceException();
            EntityOriginal = context.Set<TEntity>();
        }

        protected string TableName => typeof(TEntity).Name;

        protected DbSet<TEntity> EntityOriginal { get; }

        protected virtual IQueryable<TEntity> Entity => EntityOriginal;

        protected TContext Context { get; }

        public virtual async Task<TEntity> GetByIdOrDefaultAsync(TKey id)
        {
            var model = new TModel
            {
                Ids = new[] { id }
            };

            var query = model.GetQueryable(Context);
            return await query.FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public virtual async Task<TEntity> GetFirstOrDefaultAsync(TModel model)
        {
            var query = model.GetQueryable(Context);
            return await query.FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public virtual async Task<TEntity> GetLastOrDefaultAsync(TModel model)
        {
            var query = model.GetQueryable(Context);
            return await query.OrderByDescending(x => x.Id).FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public virtual async Task<long> GetCountAsync(TModel model)
        {
            var query = model.GetQueryable(Context);
            return await query.CountAsync().ConfigureAwait(false);
        }

        public virtual async Task<List<TEntity>> GetAsync(TExtendedModel model)
        {
            var query = model.GetQueryable(Context);
            return await query.ToListAsync().ConfigureAwait(false);
        }

        public virtual async Task<bool> IsExistAsync(TModel model)
        {
            var query = model.GetQueryable(Context);
            return await query.AnyAsync().ConfigureAwait(false);
        }

        public virtual async Task AddAsync(TEntity row)
        {
            row.CreatedAt = DateTime.Now;
            row.UpdatedAt = row.CreatedAt;

            await EntityOriginal.AddAsync(row).ConfigureAwait(false);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }

        public virtual async Task AddAsync(IEnumerable<TEntity> rows)
        {
            foreach (var row in rows)
            {
                row.CreatedAt = DateTime.Now;
                row.UpdatedAt = row.CreatedAt;
            }

            await EntityOriginal.AddRangeAsync(rows).ConfigureAwait(false);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }

        public virtual async Task ModifyAsync(TEntity row)
        {
            row.UpdatedAt = DateTime.Now;

            // NOTE: Внедрена единая логика работы с изменениям между методами ModifyAsync, ModifyAsync
            EntityOriginal.Update(row);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }

        public virtual async Task ModifyAsync(IEnumerable<TEntity> rows)
        {
            foreach (var row in rows)
            {
                row.UpdatedAt = DateTime.Now;
            }

            EntityOriginal.UpdateRange(rows);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }

        public virtual async Task DeleteAsync(TModel model)
        {
            var query = model.GetQueryable(Context);
            EntityOriginal.RemoveRange(query);

            await Context.SaveChangesAsync().ConfigureAwait(false);
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            EntityOriginal.Remove(entity);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }

        public virtual async Task DeleteAsync(IEnumerable<TEntity> entities)
        {
            EntityOriginal.RemoveRange(entities);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
