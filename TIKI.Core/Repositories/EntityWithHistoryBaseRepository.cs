﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TIKI.Core.Entities;
using TIKI.Core.Factories;
using TIKI.Core.Helpers;
using TIKI.Core.Repositories.Models;

namespace TIKI.Core.Repositories
{
    public abstract class EntityWithHistoryBaseRepository<TEntity, TKey, TModel, TExtendedModel, TEntityWithHistory, TContext>
        : BaseRepository<TEntity, TKey, TModel, TExtendedModel, TContext>
        where TContext : DbContext
        where TEntity : EntityBase<TKey>, IEntityWithHistories, new()
        where TModel : IBaseModel<TEntity, TKey, TContext>, new()
        where TExtendedModel : IExtendedBaseModel<TEntity, TKey, TContext>, new()
        where TEntityWithHistory : EntityBase<TKey>, IEntityHistories<TEntity, TKey>, new()
    {
        private readonly IMapper mapper;
        private readonly ITransactionFactory<TContext> transactionFactory;

        protected EntityWithHistoryBaseRepository(
            TContext context,
            IMapper mapper,
            ITransactionFactory<TContext> transactionFactory)
            : base(context)
        {
            this.mapper = mapper;
            this.transactionFactory = transactionFactory;
            EntityWithHistories = context.Set<TEntityWithHistory>();
        }

        protected DbSet<TEntityWithHistory> EntityWithHistories { get; }

        public override async Task AddAsync(TEntity row)
        {
            await TransactionHelper.InvokeWithEmbeddedTransaction(
                async () =>
            {
                await base.AddAsync(row);
                await AddHistory(row);
            }, transactionFactory);
        }

        public override async Task AddAsync(IEnumerable<TEntity> rows)
        {
            await TransactionHelper.InvokeWithEmbeddedTransaction(
                async () =>
                {
                    await base.AddAsync(rows);
                    await AddHistory(rows);
                }, transactionFactory);
        }

        public override async Task ModifyAsync(TEntity row)
        {
            await TransactionHelper.InvokeWithEmbeddedTransaction(
                async () =>
                {
                    await AddHistory(row);
                    await base.ModifyAsync(row);
                }, transactionFactory);
        }

        public override async Task ModifyAsync(IEnumerable<TEntity> rows)
        {
            await TransactionHelper.InvokeWithEmbeddedTransaction(
                async () =>
                {
                    await AddHistory(rows);
                    await base.ModifyAsync(rows);
                }, transactionFactory);
        }

        public override async Task DeleteAsync(TModel model)
        {
            await TransactionHelper.InvokeWithEmbeddedTransaction(
                async () =>
                {
                    //TODO не очень уверен, что сделал верно с точки зрения архитектуры
                    var entities = model.GetQueryable(Context).ToList();
                    await AddHistory(entities);
                    await DeleteAsync(entities);
                }, transactionFactory);
        }

        public override async Task DeleteAsync(TEntity entity)
        {
            await TransactionHelper.InvokeWithEmbeddedTransaction(
                async () =>
                {
                    await AddHistory(entity);
                    await base.DeleteAsync(entity);
                }, transactionFactory);
        }

        public override async Task DeleteAsync(IEnumerable<TEntity> entities)
        {
            await TransactionHelper.InvokeWithEmbeddedTransaction(
                async () =>
                {
                    await AddHistory(entities);
                    await base.DeleteAsync(entities);
                }, transactionFactory);
        }

        private async Task AddHistory(TEntity row)
        {
            var entityWithHistory = mapper.Map<TEntityWithHistory>(row);

            entityWithHistory.MainEntityId = row.Id;
            row.OptimisticLock++;
            entityWithHistory.OptimisticLock++;
            await EntityWithHistories.AddAsync(entityWithHistory);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }

        private async Task AddHistory(IEnumerable<TEntity> rows)
        {
            var entitiesWithHistory = rows.Select(row =>
            {
                var entityWithHistory = mapper.Map<TEntityWithHistory>(row);

                entityWithHistory.MainEntityId = row.Id;
                row.OptimisticLock++;
                entityWithHistory.OptimisticLock++;
                return entityWithHistory;
            });

            await EntityWithHistories.AddRangeAsync(entitiesWithHistory);
            await Context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
