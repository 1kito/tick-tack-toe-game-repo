﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TIKI.Core.Entities;
using TIKI.Core.Repositories.Models;

namespace TIKI.Core.Interfaces
{
    public interface IBaseRepository<TEntity, in TKey, TModel, TExtendedModel, TContext>
        where TContext : DbContext
        where TModel : IBaseModel<TEntity, TKey, TContext>, new()
        where TEntity : IEntityBase<TKey>, new()
        where TExtendedModel : IExtendedBaseModel<TEntity, TKey, TContext>, new()
    {
        Task<TEntity> GetByIdOrDefaultAsync(TKey id);

        Task<TEntity> GetFirstOrDefaultAsync(TModel model);

        Task<TEntity> GetLastOrDefaultAsync(TModel model);

        Task<long> GetCountAsync(TModel model);

        Task<List<TEntity>> GetAsync(TExtendedModel model);

        Task<bool> IsExistAsync(TModel model);

        Task DeleteAsync(TModel model);

        Task DeleteAsync(TEntity entity);

        Task DeleteAsync(IEnumerable<TEntity> entities);

        Task ModifyAsync(TEntity row);

        Task ModifyAsync(IEnumerable<TEntity> rows);

        Task AddAsync(IEnumerable<TEntity> rows);

        Task AddAsync(TEntity row);
    }
}
