﻿using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace TIKI.Core.Factories
{
    public class TransactionFactory<TContext> : ITransactionFactory<TContext>
        where TContext : DbContext
    {
        private readonly TContext context;

        public TransactionFactory(TContext context)
        {
            this.context = context;
        }

        /// <inheritdoc />
        public bool IsExistCurrentTransaction => context.Database.CurrentTransaction != null;

        /// <inheritdoc />
        public async Task<IDbContextTransaction> OpenTransactionAsync()
        {
            IDbContextTransaction transaction = null;
            try
            {
                transaction = await context.Database.BeginTransactionAsync();
            }
            catch (Exception)
            {
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                throw;
            }

            return transaction;
        }

        /// <inheritdoc />
        public IDbContextTransaction OpenTransaction()
        {
            IDbContextTransaction transaction = null;
            try
            {
                transaction = context.Database.BeginTransaction();
            }
            catch (Exception)
            {
                transaction?.RollbackAsync();
                throw;
            }

            return transaction;
        }

        /// <inheritdoc />
        public async Task<IDbContextTransaction> OpenTransactionAsync(IsolationLevel isolationLevel)
        {
            IDbContextTransaction transaction = null;
            try
            {
                transaction = await context.Database.BeginTransactionAsync(isolationLevel);
            }
            catch (Exception)
            {
                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                throw;
            }

            return transaction;
        }

        /// <inheritdoc />
        public IDbContextTransaction OpenTransaction(IsolationLevel isolationLevel)
        {
            IDbContextTransaction transaction = null;
            try
            {
                transaction = context.Database.BeginTransaction(isolationLevel);
            }
            catch (Exception)
            {
                transaction?.RollbackAsync();
                throw;
            }

            return transaction;
        }
    }
}
