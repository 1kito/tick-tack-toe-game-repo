﻿using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace TIKI.Core.Factories
{
    public interface ITransactionFactory<TContext>
        where TContext : DbContext
    {
        /// <summary>
        /// Проверяет есть сейчас открытая транзакция или нет
        /// </summary>
        bool IsExistCurrentTransaction { get; }

        Task<IDbContextTransaction> OpenTransactionAsync();

        IDbContextTransaction OpenTransaction();

        Task<IDbContextTransaction> OpenTransactionAsync(IsolationLevel isolationLevel);

        IDbContextTransaction OpenTransaction(IsolationLevel isolationLevel);
    }
}