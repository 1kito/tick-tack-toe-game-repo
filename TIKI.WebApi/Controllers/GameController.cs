﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using TIKI.Core.VM;
using TIKI.Data.Repositories.ExtendedModels;
using TIKI.Data.RepositoryInterfaces;

namespace TIKI.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GameController : ControllerBase
    {
        private readonly ILogger<GameController> _logger;
        private readonly IAspNetUserRepository aspNetUserRepository;

        public GameController(ILogger<GameController> logger, IAspNetUserRepository aspNetUserRepository)
        {
            _logger = logger;
            this.aspNetUserRepository = aspNetUserRepository;
        }

        [HttpGet("loadGame")]
        public IEnumerable<GamePoint> GetBoard()
        {
            var rng = new Random();
            var gp = Enumerable.Range(1, 5).Select(index => new GamePoint
            {
                Id = rng.Next(10000, 30000).ToString(),
                X = rng.Next(-20, 55),
                Y = index,
                IsX = true
            }).ToArray();
            try
            {
                var user = aspNetUserRepository.GetAsync(new ExtendedAspNetUserModel { Ids = new string[] { "e2552bef-2fce-4bc9-8bc4-4fcacf991b28" } }).Result;
            }
            catch (Exception e)
            {

            }
            // aspNetUserRepository.ModifyAsync(new AspNetUser() { });
            return gp;
        }
    }
}
