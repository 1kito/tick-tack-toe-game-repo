using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIKI.Data.Repositories;
using TIKI.Data.RepositoryInterfaces;
using TIKI.WebApi.Extensions;

namespace TIKI.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.RegRepositories();
            //services.AddCors(c =>
            //{
            //    c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            //});

            // ��������� ������� CORS, ���� ����������� ������
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder => builder
                    .WithOrigins(
                        "http://tiki.1gb.ru",
                        "http://tiki-api.1gb.ru",
                        "http://localhost:45882",
                        "http://localhost:5053")
                    .WithMethods(
                        "POST", "OPTIONS", "GET")
                    .WithHeaders("Origin", "X-Requested-With", "Content-Type", "Accept", "Access-Control-Allow-Headers", "Access-Control-Allow-Origin", "Access-Control-Allow-Methods")
                    .AllowCredentials()
                );
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(
               options =>
                   options
                       .WithOrigins(
                           "http://tiki.1gb.ru",
                           "http://tiki-api.1gb.ru",
                           "http://localhost:45882",
                           "http://localhost:5053")
                       .WithMethods(
                           "POST", "OPTIONS", "GET")
                       .WithHeaders("Origin", "X-Requested-With", "Content-Type", "Accept", "Access-Control-Allow-Headers", "Access-Control-Allow-Origin", "Access-Control-Allow-Methods")
                       .AllowCredentials());

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
