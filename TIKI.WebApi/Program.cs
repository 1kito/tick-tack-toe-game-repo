using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TIKI.Core.Constants;
using TIKI.Data;

namespace TIKI.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger
                = NLogBuilder.ConfigureNLog(GlobalConstants.NLogConfig).GetCurrentClassLogger();

            try
            {
                logger.Debug("init main");

                var host = CreateHostBuilder(args).Build();

                TickTackToeDbMigrate.Migrate(host, logger);

                host.Run();
            }
            catch (Exception e)
            {
                logger.Error(e, "Stopped program because of exception.");
                throw;
            }

            // CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
