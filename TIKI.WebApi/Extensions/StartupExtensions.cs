﻿using Microsoft.Extensions.DependencyInjection;
using TIKI.Data;
using TIKI.Data.Repositories;
using TIKI.Data.RepositoryInterfaces;

namespace TIKI.WebApi.Extensions
{
    public static class StartupExtensions
    {
        public static void RegRepositories(this IServiceCollection services)
        {
            services.AddDbContext<TickTackToeContext>();
            services.AddTransient<IAspNetUserRepository, AspNetUserRepository>();
        }
    }
}
