﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbMigrations
{
    [Migration(201506201340)]
    public class Migration_2015_06_20_13_40 : MigrationWithConstants
    {
        public override void Up()
        {
            #region Create Tables

            Create.Table(AppSettings).InSchema(dbo)
                  .WithColumn(ID).AsInt32().Identity().NotNullable().PrimaryKey()
                  .WithColumn("Key").AsString(255).NotNullable()
                  .WithColumn("Value").AsString(-1).NotNullable();

            Create.Table(Games).InSchema(dbo)
              .WithColumn(ID).AsInt32().Identity().NotNullable().PrimaryKey()
              .WithColumn(OnlineGamerID).AsInt32().NotNullable() //  .WithColumn(OnlineGamerID).AsInt32().Nullable()
              .WithColumn("GUID").AsString(36).NotNullable()
              .WithColumn("PartGUID").AsString(36).NotNullable() // номер серии игр - в случае, если игр много.
              .WithColumn(WhoseTurnID).AsString(128).NotNullable() // чей ход ожидается
              .WithColumn(SecondGamerID).AsString(128).Nullable()
              .WithColumn(WinnerID).AsString(128).Nullable()
              .WithColumn("FieldSize").AsInt32().NotNullable() // размер поля
              .WithColumn("WinSize").AsInt32().NotNullable() // выигрышный размер, по умолчанию 3
              .WithColumn("Enabled").AsBoolean().NotNullable() // игра блокирована по причине прерванности, либо окончена
              .WithColumn("Started").AsDateTime().Nullable()
              .WithColumn("Finished").AsDateTime().Nullable()
              .WithColumn("Created").AsDateTime().NotNullable()
              .WithColumn("Modified").AsDateTime().NotNullable()
              .WithColumn("Deleted").AsBoolean().NotNullable();

            Create.Table(OnlineGamers).InSchema(dbo)
                .WithColumn(ID).AsInt32().Identity().NotNullable().PrimaryKey()
                .WithColumn(UserID).AsString(128).NotNullable()
                .WithColumn("IP").AsString(128).Nullable()
                .WithColumn("BrowserInfo").AsString(-1).Nullable()
                .WithColumn("Started").AsDateTime().Nullable() // время захода на сайт - статус онлайн 
                .WithColumn("Finished").AsDateTime().Nullable() // время окончания онлайн
                .WithColumn("Created").AsDateTime().NotNullable()
                .WithColumn("Modified").AsDateTime().NotNullable()
                .WithColumn("Deleted").AsBoolean().NotNullable();

            #endregion

            #region Create Foreign Keys

            CreateFK(Games, OnlineGamers, OnlineGamerID, ID);
            CreateFK(OnlineGamers, AspNetUsers, UserID, Id);

            #endregion
        }

        public override void Down()
        {
            #region Delete Foreign Keys

            Delete.ForeignKey(FK(Games, OnlineGamers)).OnTable(Games).InSchema(dbo);
            Delete.ForeignKey(FK(OnlineGamers, AspNetUsers)).OnTable(OnlineGamers).InSchema(dbo);            

            #endregion

            #region Delete Tables

            Delete.Table(AppSettings).InSchema(dbo);
            Delete.Table(Games).InSchema(dbo);
            Delete.Table(OnlineGamers).InSchema(dbo);

            #endregion
        }
    }
}
