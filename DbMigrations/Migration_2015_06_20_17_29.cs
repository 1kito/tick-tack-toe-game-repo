﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbMigrations
{
    [Migration(201506201729)]
    public class Migration_2015_06_20_17_29 : MigrationWithConstants
    {
        public override void Up()
        {
            #region Create Tables

            Create.Table(GameTurns).InSchema(dbo)
              .WithColumn(ID).AsInt32().Identity().NotNullable().PrimaryKey()
              .WithColumn(GameID).AsInt32().NotNullable()
              .WithColumn(UserID).AsString(128).NotNullable()
              .WithColumn("Vertical").AsInt32().NotNullable()
              .WithColumn("Horizontal").AsInt32().NotNullable()
              .WithColumn(Created).AsDateTime().NotNullable()
              .WithColumn(Modified).AsDateTime().NotNullable()
              .WithColumn(Deleted).AsBoolean().NotNullable();

            #endregion

            #region Create Foreign Keys

            CreateFK(GameTurns, Games, GameID, ID);
            CreateFK(GameTurns, AspNetUsers, UserID, Id);

            #endregion
        }

        public override void Down()
        {
            #region Delete Foreign Keys

            Delete.ForeignKey(FK(GameTurns, Games)).OnTable(GameTurns).InSchema(dbo);
            Delete.ForeignKey(FK(GameTurns, AspNetUsers)).OnTable(GameTurns).InSchema(dbo);

            #endregion

            #region Delete Tables

            Delete.Table(GameTurns).InSchema(dbo);

            #endregion
        }
    }
}
