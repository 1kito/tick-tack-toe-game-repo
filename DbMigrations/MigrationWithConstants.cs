﻿using FluentMigrator;
using System;

namespace DbMigrations
{
    public abstract class MigrationWithConstants : Migration
    {
        // main
        public const string dbo = "dbo";
        public const string FK_0_1 = "FK_{0}_{1}";

        // columns
        public const string Id = "Id";
        public const string ID = "ID";
        public const string UserID = "UserID";
        public const string GameID = "GameID";
        public const string OnlineGamerID = "OnlineGamerID";
        public const string WhoseTurnID = "WhoseTurnID";
        public const string SecondGamerID = "SecondGamerID";
        public const string WinnerID = "WinnerID";   
    
        public const string Created = "Created";   
        public const string Modified = "Modified";   
        public const string Deleted = "Deleted";   

        // tables
        public const string AppSettings = "AppSettings";
        public const string AspNetUsers = "AspNetUsers";
        public const string OnlineGamers = "OnlineGamers";
        public const string Games = "Games";
        public const string GameTurns = "GameTurns";
       

        /// <summary>
        /// String.Format("FK_{0}_{1}", first, second)
        /// </summary>
        /// <param name="foreign">таблица с внешним ключом</param>
        /// <param name="primary">таблица с первичным ключом</param>
        /// <returns></returns>
        public string FK(string foreign, string primary, string format = FK_0_1)
        {
            return String.Format(format, foreign, primary);
        }

        public void CreateFK(string foreign, string primary, string foreignkey, string primarykey = ID, string format = FK_0_1)
        {
            Create.ForeignKey(FK(foreign, primary, format))
              .FromTable(foreign).InSchema(dbo).ForeignColumns(foreignkey)
              .ToTable(primary).InSchema(dbo).PrimaryColumns(primarykey);
        }
    }
}
